import dj_database_url
import os

from ipaddress import IPv4Network

from settings.mycoscope_settings import *  # NOQA


ALLOWED_HOSTS = os.environ["ALLOWED_HOSTS"].split(",")
DATABASES = {
    "default": dj_database_url.parse(
        os.environ["DATABASE_URL"], test_options={"NAME": "test_db"}
    )
}
EXEMPT_2FA_NETWORKS = [IPv4Network("0.0.0.0/0")]
DEBUG = True
SECRET_KEY = "no secret here"
