from settings._base_settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME':   'mycoscope',
        'USER':   'myco',
        'PASSWORD': '123456',
    }
}

LANGUAGES = (
    ('en', 'English'),
    ('fr', 'French'),
)

CREATE_TAXONOMY_ON_IMPORT = False
SITE_ADMIN_GROUPS = ['myco_admin']
WEB_STATS_URL = "/awstats/awstats.pl?config=mycoscope"

try:
    # Load optional local settings (mainly for non-versioned content like passwords)
    from local_settings import *
except ImportError:
    pass
