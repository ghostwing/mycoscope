# Django settings for bcis project.
from pathlib import Path
from django.urls import reverse_lazy

DEBUG = True
BASE_DIR = Path(__file__).resolve().parent.parent

ADMINS = (
    ('Claude Paroz', 'claude@2xlibre.net'),
)

MANAGERS = ADMINS
SERVER_EMAIL = "server@bcis.ch"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME':   'bcis',
        'USER':   'bcis',
        'PASSWORD': '123456',
    }
}
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
TIME_ZONE = 'Europe/Zurich'
USE_TZ = True

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'
LANGUAGES = (
    ('en', 'English'),
    ('fr', 'French'),
    ('de', 'German'),
)

USE_I18N = True

# Absolute path to the directory that holds media.
MEDIA_ROOT = BASE_DIR / 'media'

# URL that handles the media served from MEDIA_ROOT.
MEDIA_URL = '/media/'

STATIC_ROOT = BASE_DIR / 'static'
STATIC_URL = '/static/'

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'policy.middleware.PolicyMiddleware',
]

ROOT_URLCONF = 'common.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
            ],
            'builtins': [
                'django.templatetags.static',
            ]
        },
    },
]

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # External apps
    'mptt',
    'sorl.thumbnail',
    'tinymce',
    # BCIS apps
    'policy',
    'proj',
    'permission',
    'invent',
    'imports',
    'taxo',
    'bioseq',
    'publications',
]
LOGIN_URL = "/login/"
LOCALE_PATHS = [BASE_DIR / 'locale']

MESSAGE_STORAGE = 'django.contrib.messages.storage.fallback.FallbackStorage'

ABSOLUTE_URL_OVERRIDES = {
    'auth.user': lambda o: reverse_lazy('profile'),
}

# Force all uploaded files to be written on disk, so as they can be opened by xlrd module
FILE_UPLOAD_HANDLERS = ("django.core.files.uploadhandler.TemporaryFileUploadHandler",)

CREATE_TAXONOMY_ON_IMPORT = True
ALLOW_TABULAR_VIEW = 'admin' # all, auth, admin
SITE_ADMIN_GROUPS = []
WEB_STATS_URL = ""

# Setting for bioseq application
BLASTDB_PATH = "/tmp/blast.db"

TINYMCE_DEFAULT_CONFIG = {
    'theme': 'silver',
    'menubar': False,
    'plugins': 'link code',
    'toolbar1': 'bold italic | link unlink | undo redo | code',
    'toolbar_location': 'bottom',
    'statusbar': False,
    'icons': 'small',
}
