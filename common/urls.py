from django.conf import settings
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import include, path, re_path
from django.utils import timezone
from django.views.decorators.http import last_modified
from django.views.i18n import JavaScriptCatalog
from django.views.static import serve

from invent.forms import ImageSearchForm
from invent import views
from proj import views as proj_views
from policy import views as pol_views
from publications import views as publications_views

handler500 = 'proj.views.server_error'
last_modified_date = timezone.now()

urlpatterns = [
    path('admin/', admin.site.urls),
    path('taxo/',  include('apps.taxo.urls')),
    path('seq/', include('bioseq.urls')),
    path('tinymce/', include('tinymce.urls')),

    #path('register/', proj_views.site_register, name='register'),
    #path('register/success/', TemplateView.as_view(template_name='proj/register_success.html'), name='register_success'),
    #path('register/activate/<key>/', proj_views.activate_account, name='register_activation'),
    path('login/', auth_views.LoginView.as_view(template_name='proj/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),
    path('accounts/profile/', proj_views.profile, name='profile'),
    path('password_change/', auth_views.PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    re_path(r'^setlang/(?P<lang>(fr|en))/$', proj_views.set_language, name='setlang'),
    path('site_admin/', proj_views.site_admin, name='site_admin'),
    path('help/', proj_views.help_index, name='help_index'),
    path('help/<subject>/', proj_views.help, name='help'),

    path('jsi18n/', last_modified(lambda req, **kw: last_modified_date)(JavaScriptCatalog.as_view()),
        name='javascript_catalog'),

    path('policy/', pol_views.accept_policy, name='accept_policy'),
    path('', views.IndexView.as_view(), name='home'),

    path('search/', views.SearchView.as_view(), name='search'),
    path('search/export', views.SearchView.as_view(output='export'), name='search-export'),
    path('search/add_criterion/<int:base_num>/',
        views.add_criterion, name='add_criterion'),
    path('search_images/', views.SearchView.as_view(form_class=ImageSearchForm, template_name='search/search_images.html'),
        name='search_img'),

    path('invent/', views.invent_list, name='inventories'),
    path('autocomplete/', views.autocomplete, name='autocomplete'),
    path('invent/<int:parent_id>/new/<obj_code>/', views.ObjectCreateView.as_view(), name='object_create'),
    path('invent/<int:obj_id>/', views.object_detail, name='object_detail'),
    path('invent/<int:obj_id>/as_image/', views.object_image, name='object_image'),
    path('invent/<int:obj_id>/edit/', views.ObjectEditView.as_view(), name='object_edit'),
    path('invent/<int:obj_id>/editperms/', views.object_edit_perms, name='object_edit_perms'),
    path('invent/<int:obj_id>/delete/', views.object_delete, name='object_delete'),
    re_path(r'^invent/(?P<obj_id>\d+)/contents/(?P<otype>[-\w]+)?/?$',
        views.PaginatedObjectsView.as_view(), name='object_list'),
    path('invent/<int:obj_id>/reverse_refs/',
        views.PaginatedReverseObjectsView.as_view(), name='reverse_objects'),
    re_path(r'^invent/(?P<obj_id>\d+)/contents/(?P<otype>[-\w]+)/(?P<format>(table|geojson))/$',
        views.object_list_table, name='object_list_table'),
    re_path(r'^invent/by_uuid/(?P<uuid>[-\w]+)/(?P<format>(html|json|xml))?/?$',
        views.object_list_by_uuid, name='object_by_uuid'),
    path('invent/<int:obj_id>/import/', views.object_import, name='object_import'),
    path('attributes/', views.attributes, name='attributes'),
    # attribute name passed in GET
    path('attributes/vocab/', views.vocab_for_attribute, name='vocab_for_attribute'),

    re_path(r'^attributes_in_group/(?P<group_id>-?\d+)/$', views.attributes_in_group),

    path('<int:object_id>/value/new/<int:attr_id>/', views.add_value, name='add_value'),
    re_path(r'^(?P<object_id>\d+)/value/(?P<value_id>[\w\-]+)/edit/$', views.edit_value),
    path('<int:object_id>/value/del/', views.del_value, name='del_value'), # value id in POST
    # Image and file attachment editing
    path('<int:object_id>/photo/add/', views.add_photo, name="add_photo"),
    path('<int:object_id>/photo/<int:photo_id>/edit/', views.edit_photo),
    path('<int:object_id>/photo/del/', views.del_photo), # photo id in POST
    path('<int:object_id>/file/<int:file_id>/edit/', views.edit_file, name='edit_file'),
    path('<int:object_id>/file/del/', views.del_file), # file id in POST

    path('publications/', publications_views.PublicationsView.as_view(), name='publications'),
    path('press-corner/', publications_views.PressCornerView.as_view(), name='press_corner'),
]

if settings.DEBUG:
    urlpatterns += [
        path('media/<path:path>', serve, {'document_root': settings.MEDIA_ROOT}),
    ]
