# This code is part of the Biopython distribution and governed by its
# license.  Please see the LICENSE file that should have been included
# as part of this package.
# This test file is taken frombiopython/Tests/test_BioSQL.py,
# stripped from the BioSQL dependent stuff (database,...)
"""Tests for dealing with storage of biopython objects in a relational db.
"""
# standard library
import os
from io import StringIO
from unittest import skip, skipUnless

from django.test import TestCase
from django.db import IntegrityError
from django.conf import settings

# local stuff
from Bio import MissingExternalDependencyError
from Bio.Seq import Seq, MutableSeq
from Bio.SeqFeature import SeqFeature
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

#from BioSQL import BioSeqDatabase
#from BioSQL import BioSeq
from bioseq import models
from bioseq.models import DBSeq

from .seq_tests_common import compare_record, compare_records
TEST_DATA_PATH = "/home/claude/checkouts/biopython/Tests/"

def load_database(gb_handle):
    """Load a GenBank file into a new BioSQL database.
    
    This is useful for running tests against a newly created database.
    """

    db = models.BioDatabase(name="biosql-test", description="This is a test database")
    db.save()
    db.load(SeqIO.parse(gb_handle, "gb"))
    return db


@skipUnless('bioseq' in settings.INSTALLED_APPS, 'bioseq app is not installed')
class BioPythonBaseTestCase(TestCase):
    fixtures = ['initial_data.json']


@skip("miss 'gb' files")
class ReadTest(BioPythonBaseTestCase):
    """Test reading a database from an already built database.
    """
    loaded_db = 0
    
    def setUp(self):
        """Connect to and load up the database.
        """
        gb_file = os.path.join(TEST_DATA_PATH, "GenBank", "cor6_6.gb")
        gb_handle = open(gb_file, "r")
        self.db = load_database(gb_handle)
        gb_handle.close()

    def test_get_db_items(self):
        """Get a list of all items in the database.
        """
        items = self.db.values()

    def test_lookup_items(self):
        """Test retrieval of items using various ids.
        """
        item = self.db.lookup(accession = "X62281")
        try:
            item = self.db.lookup(accession = "Not real")
            raise AssertionError("No problem on fake id retrieval")
        except IndexError:
            pass
        item = self.db.lookup(display_id = "ATKIN2")
        try:
            item = self.db.lookup(display_id = "Not real")
            raise AssertionError("No problem on fake id retrieval")
        except IndexError:
            pass
        
        # primary id retrieval
        item = self.db.lookup(primary_id = "16353")
        try:
            item = self.db.lookup(primary_id = "Not Real")
            raise AssertionError("No problem on fake primary id retrieval")
        except IndexError:
            pass


@skip("miss 'gb' files")
class SeqInterfaceTest(BioPythonBaseTestCase):
    """Make sure the BioSQL objects implement the expected biopython interfaces
    """
    def setUp(self):
        """Load a database.
        """
        gb_file = os.path.join(TEST_DATA_PATH, "GenBank", "cor6_6.gb")
        gb_handle = open(gb_file, "r")
        self.db = load_database(gb_handle)
        gb_handle.close()

        self.item = self.db.lookup(accession = "X62281")

    def test_seq_record(self):
        """Make sure SeqRecords from BioSQL implement the right interface.
        """
        test_record = self.item
        self.assertIsInstance(test_record.seq, DBSeq)
        self.assertEqual(test_record.id, "X62281.1", test_record.id)
        self.assertEqual(test_record.name, "ATKIN2")
        self.assertEqual(test_record.description, "A.thaliana kin2 gene")
        self.assertTrue(hasattr(test_record, 'annotations'))
        # XXX should do something with annotations once they are like
        # a dictionary
        for feature in test_record.features:
            self.assertIsInstance(feature, SeqFeature)
        s = str(test_record) #shouldn't cause any errors!

    def test_seq(self):
        """Make sure Seqs from BioSQL implement the right interface.
        """
        test_seq = self.item.seq
        data = test_seq.data
        self.assertEqual(type(data), type(""))
        string_rep = str(test_seq)
        self.assertEqual(string_rep, str(test_seq)) #check __str__ too
        self.assertEqual(type(string_rep), type(""))
        self.assertEqual(len(test_seq), 880)
        
    def test_convert(self):
        """Check can turn a DBSeq object into a Seq or MutableSeq."""
        test_seq = self.item.seq

        other = test_seq.toseq()
        self.assertEqual(str(test_seq), str(other))
        self.assertIsInstance(other, Seq)

        other = MutableSeq(test_seq)
        self.assertEqual(str(test_seq), str(other))
        self.assertIsInstance(other, MutableSeq)

    def test_addition(self):
        """Check can add DBSeq objects together."""
        test_seq = self.item.seq
        for other in [Seq("ACGT"),
                      MutableSeq("ACGT"),
                      "ACGT",
                      test_seq]:
            test = test_seq + other
            self.assertEqual(str(test), str(test_seq) + str(other))
            self.assertIsInstance(test, Seq)
            test = other + test_seq
            self.assertEqual(str(test), str(other) + str(test_seq))

    def test_seq_slicing(self):
        """Check that slices of sequences are retrieved properly.
        """
        test_seq = self.item.seq
        new_seq = test_seq[:10]
        #self.assert_(isinstance(new_seq, DBSeq))
        # Currently, sliced sequences return Seq and not DBSeq...
        self.assertIsInstance(new_seq, Seq)
        # simple slicing
        self.assertEqual(str(test_seq[:5]), 'ATTTG')
        self.assertEqual(str(test_seq[0:5]), 'ATTTG')
        self.assertEqual(str(test_seq[2:3]), 'T')
        self.assertEqual(str(test_seq[2:4]), 'TT')
        self.assertEqual(str(test_seq[870:]), 'TTGAATTATA')
        # getting more fancy
        self.assertEqual(test_seq[-1], 'A')
        self.assertEqual(test_seq[1], 'T')
        self.assertEqual(str(test_seq[-10:][5:]), "TTATA")
        self.assertEqual(str(test_seq[-10:][5:]), "TTATA")

    def test_seq_features(self):
        """Check SeqFeatures of a sequence.
        """
        test_features = self.item.features
        cds_feature = test_features[6]
        self.assertEqual(cds_feature.type, "CDS")
        self.assertEqual(str(cds_feature.location),
                         "join{[103:160](+), [319:390](+), [503:579](+)}")

        try:
            self.assertEqual(cds_feature.qualifiers["gene"], ["kin2"])
            self.assertEqual(cds_feature.qualifiers["protein_id"], ["CAA44171.1"])
            self.assertEqual(cds_feature.qualifiers["codon_start"], ["1"])
        except KeyError:
            raise KeyError("Missing expected entries, have %s" \
                           % repr(cds_feature.qualifiers))
        
        self.assertIn("db_xref", cds_feature.qualifiers)
        multi_ann = cds_feature.qualifiers["db_xref"]
        self.assertEqual(len(multi_ann), 2)
        self.assertIn("GI:16354", multi_ann)
        self.assertIn("SWISS-PROT:P31169", multi_ann)


@skip("miss 'gb' files")
class LoaderTest(BioPythonBaseTestCase):
    """Load a database from a GenBank file.
    """
    def setUp(self):
        
        self.db = models.BioDatabase(name="biosql-test", description="This is a test database")
        self.db.save()

        # get the GenBank file we are going to put into it
        input_file = os.path.join(TEST_DATA_PATH, "GenBank", "cor6_6.gb")
        handle = open(input_file, "r")
        self.iterator = SeqIO.parse(handle, "gb")

    def test_load_database(self):
        """Load SeqRecord objects into a BioSQL database.
        """
        self.db.load(self.iterator)

        # do some simple tests to make sure we actually loaded the right
        # thing. More advanced tests in a different module.
        items = self.db.values()
        self.assertEqual(len(items), 6)
        item_names = []
        item_ids = []
        for item in items:
            item_names.append(item.name)
            item_ids.append(item.id)
        item_names.sort()
        item_ids.sort()
        self.assertEqual(item_names, ['AF297471', 'ARU237582', 'ATCOR66M',
                                      'ATKIN2', 'BNAKINI', 'BRRBIF72'])
        self.assertEqual(item_ids, ['AF297471.1', 'AJ237582.1', 'L31939.1',
                                    'M81224.1', 'X55053.1', 'X62281.1'])


class DupLoadTest(BioPythonBaseTestCase):
    """Check a few duplicate conditions fail."""
    def setUp(self):
        self.db = models.BioDatabase(name="biosql-test", description="This is a test database")
        self.db.save()

    def test_duplicate_load(self):
        """Make sure can't import a single record twice (in one go)."""
        record = SeqRecord(Seq("ATGCTATGACTAT"),id="Test1")
        self.assertRaises(IntegrityError, self.db.load, [record,record])

    def test_duplicate_load2(self):
        """Make sure can't import a single record twice (in steps)."""
        record = SeqRecord(Seq("ATGCTATGACTAT"),id="Test2")
        count = len(self.db.load([record]))
        self.assertEqual(count,1)
        self.assertRaises(IntegrityError, self.db.load, [record])

    def test_duplicate_id_load(self):
        """Make sure can't import records with same ID (in one go)."""
        record1 = SeqRecord(Seq("ATGCTATGACTAT"),id="TestA")
        record2 = SeqRecord(Seq("GGGATGCGACTAT"),id="TestA")
        self.assertRaises(IntegrityError, self.db.load, [record1,record2])


@skip("miss 'gb' files")
class ClosedLoopTest(BioPythonBaseTestCase):
    """Test file -> BioSQL -> file."""
    def setUp(self):
        self.db = models.BioDatabase(name="biosql-test", description="This is a test database")
        self.db.save()
    
    def test_NC_005816(self):
        """GenBank file to BioSQL and back to a GenBank file, NC_005816."""
        self.loop(os.path.join(TEST_DATA_PATH, "GenBank", "NC_005816.gb"), "gb")

    def test_NC_000932(self):
        """GenBank file to BioSQL and back to a GenBank file, NC_000932."""
        self.loop(os.path.join(TEST_DATA_PATH, "GenBank", "NC_000932.gb"), "gb")

    def test_NT_019265(self):
        """GenBank file to BioSQL and back to a GenBank file, NT_019265."""
        self.loop(os.path.join(TEST_DATA_PATH, "GenBank", "NT_019265.gb"), "gb")

    def test_protein_refseq2(self):
        """GenBank file to BioSQL and back to a GenBank file, protein_refseq2."""
        self.loop(os.path.join(TEST_DATA_PATH, "GenBank", "protein_refseq2.gb"), "gb")

    def test_no_ref(self):
        """GenBank file to BioSQL and back to a GenBank file, noref."""
        self.loop(os.path.join(TEST_DATA_PATH, "GenBank", "noref.gb"), "gb")

    def test_one_of(self):
        """GenBank file to BioSQL and back to a GenBank file, one_of."""
        self.loop(os.path.join(TEST_DATA_PATH, "GenBank", "one_of.gb"), "gb")

    def test_cor6_6(self):
        """GenBank file to BioSQL and back to a GenBank file, cor6_6."""
        self.loop(os.path.join(TEST_DATA_PATH, "GenBank", "cor6_6.gb"), "gb")

    def test_arab1(self):
        """GenBank file to BioSQL and back to a GenBank file, arab1."""
        self.loop(os.path.join(TEST_DATA_PATH, "GenBank", "arab1.gb"), "gb")

    def loop(self, filename, format):
        with open(filename, "r") as fh:
            original_records = list(SeqIO.parse(fh, format))
        count = len(self.db.load(original_records))
        self.assertEqual(count, len(original_records))
        #Now read them back...
        biosql_records = [self.db.lookup(name=rec.name) \
                          for rec in original_records]
        #And check they agree
        self.assertTrue(compare_records(original_records, biosql_records))
        #Now write to a handle...
        handle = StringIO()
        SeqIO.write(biosql_records, handle, "gb")
        #Now read them back...
        handle.seek(0)
        new_records = list(SeqIO.parse(handle, "gb"))
        #And check they still agree
        self.assertEqual(len(new_records), len(original_records))
        for old, new in zip(original_records, new_records):
            #TODO - remove this hack because we don't yet write these (yet):
            for key in ["comment", "references", "db_source"]:
                if key in old.annotations and key not in new.annotations:
                    del old.annotations[key]
            #TODO - remove this hack one we write the date properly:
            del old.annotations["date"]
            del new.annotations["date"]
            self.assertTrue(compare_record(old, new))


@skip("miss 'gb' files")
class TransferTest(BioPythonBaseTestCase):
    """Test file -> BioSQL, BioSQL -> BioSQL."""
    def setUp(self):
        self.db = models.BioDatabase(name="biosql-test", description="This is a test database")
        self.db.save()
        self.db2 = models.BioDatabase(name="biosql-test2", description="This is a test database")
        self.db2.save()

    def test_NC_005816(self):
        """GenBank file to BioSQL, then again to a new namespace, NC_005816."""
        self.trans(os.path.join(TEST_DATA_PATH, "GenBank", "NC_005816.gb"), "gb")

    def test_NC_000932(self):
        """GenBank file to BioSQL, then again to a new namespace, NC_000932."""
        self.trans(os.path.join(TEST_DATA_PATH, "GenBank", "NC_000932.gb"), "gb")

    def test_NT_019265(self):
        """GenBank file to BioSQL, then again to a new namespace, NT_019265."""
        self.trans(os.path.join(TEST_DATA_PATH, "GenBank", "NT_019265.gb"), "gb")

    def test_protein_refseq2(self):
        """GenBank file to BioSQL, then again to a new namespace, protein_refseq2."""
        self.trans(os.path.join(TEST_DATA_PATH, "GenBank", "protein_refseq2.gb"), "gb")

    def test_no_ref(self):
        """GenBank file to BioSQL, then again to a new namespace, noref."""
        self.trans(os.path.join(TEST_DATA_PATH, "GenBank", "noref.gb"), "gb")

    def test_one_of(self):
        """GenBank file to BioSQL, then again to a new namespace, one_of."""
        self.trans(os.path.join(TEST_DATA_PATH, "GenBank", "one_of.gb"), "gb")

    def test_cor6_6(self):
        """GenBank file to BioSQL, then again to a new namespace, cor6_6."""
        self.trans(os.path.join(TEST_DATA_PATH, "GenBank", "cor6_6.gb"), "gb")

    def test_arab1(self):
        """GenBank file to BioSQL, then again to a new namespace, arab1."""
        self.trans(os.path.join(TEST_DATA_PATH, "GenBank", "arab1.gb"), "gb")

    def trans(self, filename, format):
        with open(filename, "r") as fh:
            original_records = list(SeqIO.parse(fh, format))
        count = len(self.db.load(original_records))
        self.assertEqual(count, len(original_records))
        #Now read them back...
        biosql_records = [self.db.lookup(name=rec.name) \
                          for rec in original_records]
        #And check they agree
        self.assertTrue(compare_records(original_records, biosql_records))
        #Now write to a second name space...
        count = len(self.db2.load(biosql_records))
        self.assertEqual(count, len(original_records))
        #Now read them back again,
        biosql_records2 = [self.db2.lookup(name=rec.name) \
                          for rec in original_records]
        #And check they also agree
        self.assertTrue(compare_records(original_records, biosql_records2))


@skip("miss 'gb' files")
class InDepthLoadTest(BioPythonBaseTestCase):
    """Make sure we are loading and retreiving in a semi-lossless fashion.
    """
    def setUp(self):
        gb_file = os.path.join(TEST_DATA_PATH, "GenBank", "cor6_6.gb")
        gb_handle = open(gb_file, "r")
        self.db = load_database(gb_handle)
        gb_handle.close()

    def test_transfer(self):
        """Make sure can load record into another namespace."""
        #Should be in database already...
        db_record = self.db.lookup(accession = "X55053")
        #Make a new namespace
        db2 = models.BioDatabase(name="biosql-test2", description="This is a test database")
        db2.save()
        #Should be able to load this DBSeqRecord there...
        count = len(db2.load([db_record]))
        self.assertEqual(len(db2.values()),1)

    def test_reload(self):
        """Make sure can't reimport existing records."""
        gb_file = os.path.join(TEST_DATA_PATH, "GenBank", "cor6_6.gb")
        gb_handle = open(gb_file, "r")
        record = next(SeqIO.parse(gb_handle, "gb"))
        gb_handle.close()
        #Should be in database already...
        db_record = self.db.lookup(accession = "X55053")
        self.assertEqual(db_record.id, record.id)
        self.assertEqual(db_record.name, record.name)
        self.assertEqual(db_record.description, record.description)
        self.assertEqual(str(db_record.seq), str(record.seq))
        #Good... now try reloading it!
        self.assertRaises(IntegrityError, self.db.load, [record])

    def test_record_loading(self):
        """Make sure all records are correctly loaded.
        """
        test_record = self.db.lookup(accession = "X55053")
        self.assertEqual(test_record.name, "ATCOR66M")
        self.assertEqual(test_record.id, "X55053.1")
        self.assertEqual(test_record.description, "A.thaliana cor6.6 mRNA")
        self.assertEqual(str(test_record.seq[:10]), 'AACAAAACAC')

        test_record = self.db.lookup(accession = "X62281")
        self.assertEqual(test_record.name, "ATKIN2")
        self.assertEqual(test_record.id, "X62281.1")
        self.assertEqual(test_record.description, "A.thaliana kin2 gene")
        self.assertEqual(str(test_record.seq[:10]), 'ATTTGGCCTA')

    def test_seq_feature(self):
        """Indepth check that SeqFeatures are transmitted through the db.
        """
        test_record = self.db.lookup(accession = "AJ237582")
        features = test_record.features
        self.assertEqual(len(features), 7)
       
        # test single locations
        test_feature = features[0]
        self.assertEqual(test_feature.type, "source")
        self.assertEqual(str(test_feature.location), "[0:206](+)")
        self.assertEqual(len(test_feature.qualifiers.keys()), 3)
        self.assertEqual(test_feature.qualifiers["country"], ["Russia:Bashkortostan"])
        self.assertEqual(test_feature.qualifiers["organism"], ["Armoracia rusticana"])
        self.assertEqual(test_feature.qualifiers["db_xref"], ["taxon:3704"])

        # test split locations
        test_feature = features[4]
        self.assertEqual(test_feature.type, "CDS")
        self.assertEqual(str(test_feature.location), "join{[0:48](+), [142:206](+)}")
        self.assertEqual(len(test_feature.location.parts), 2)
        self.assertEqual(str(test_feature.location.parts[0]), "[0:48](+)")
        self.assertEqual(str(test_feature.location.parts[1]), "[142:206](+)")
        self.assertEqual(test_feature.location.operator, "join")
        self.assertEqual(len(test_feature.qualifiers.keys()), 6)
        self.assertEqual(test_feature.qualifiers["gene"], ["csp14"])
        self.assertEqual(test_feature.qualifiers["codon_start"], ["2"])
        self.assertEqual(test_feature.qualifiers["product"],
                         ["cold shock protein"])
        self.assertEqual(test_feature.qualifiers["protein_id"], ["CAB39890.1"])
        self.assertEqual(test_feature.qualifiers["db_xref"], ["GI:4538893"])
        self.assertEqual(test_feature.qualifiers["translation"],
                         ["DKAKDAAAAAGASAQQAGKNISDAAAGGVNFVKEKTG"])

        # test passing strand information
        # XXX We should be testing complement as well
        test_record = self.db.lookup(accession = "AJ237582")
        test_feature = test_record.features[4] # DNA, no complement
        self.assertEqual(test_feature.strand, 1)
        for loc in test_feature.location.parts:
            self.assertEqual(loc.strand, 1)

        test_record = self.db.lookup(accession = "X55053")
        test_feature = test_record.features[0]
        # mRNA, so really cDNA, so the strand should be 1 (not complemented)
        self.assertEqual(test_feature.strand, 1)
