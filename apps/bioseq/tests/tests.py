import os
from unittest import skipUnless

from django.conf import settings
from django.urls import reverse

from Bio import SeqIO
from bioseq import models
from invent.models import Attribute, BaseObject
from invent.tests.base import BaseTestCase

# Tests copied from biopython/biosql
from bioseq.tests.biosql import *

test_file_dir = os.path.dirname(os.path.abspath(__file__))


@skipUnless('bioseq' in settings.INSTALLED_APPS, 'bioseq app is not installed')
class BioSeqTestCase(BaseTestCase):
    fixtures = ['initial_data.json']

    def setUp(self):
        super(BioSeqTestCase, self).setUp()
        if 'policy.middleware.PolicyMiddleware' in settings.MIDDLEWARE:
            settings.MIDDLEWARE.remove('policy.middleware.PolicyMiddleware')
        self._create_database()
        Attribute.objects.create(name="ACCENUMB", datatype="text", atitle="Accession number")

    def _create_database(self):
        self.db = models.BioDatabase(name="biosql-test", description="This is a test database")
        self.db.save()

        with open(os.path.join(test_file_dir, "ls_orchid_test.gbk"), 'r') as f:
            self.db.load(SeqIO.parse(f, "genbank"))

    def test_load_genbank_records(self):
        """ Load a file-based set of GenBank records in a BioDatabase """
        #f = open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "ls_orchid_test.gbk"), 'r')
        #self.db.load(SeqIO.parse(f, "genbank"))
        self.assertEqual(self.db.bioentry_set.count(), 5)
        #for seq_record in SeqIO.parse(f, "genbank"):
        #    print seq_record.id

    def test_import_fasta_record(self):
        obj = self._create_object(self.spec_type)
        self.client.login(username=self.user.username, password='johnpw')
        with open(os.path.join(test_file_dir, "sample.fasta"), 'r') as f:
            response = self.client.post(reverse('import_seq_file', args=[obj.pk]), data={
                'seqfile': f,
                'target': 'current',
            })
        self.assertEqual(obj.bioentry_set.count(), 1)
        bioentry = obj.bioentry_set.all()[0]
        self.assertRedirects(response, reverse('display_sequence', args=[obj.pk, bioentry.pk]))

    def test_import_multiple_fasta_record(self):
        """ Multiple FASTA entries for the same accession """
        obj = self._create_object(self.spec_type, values=(('ACCENUMB', '541'),))
        self.client.login(username=self.user.username, password='johnpw')
        with open(os.path.join(test_file_dir, "multiple.fasta"), 'r') as f:
            response = self.client.post(reverse('import_seq_file', args=[obj.pk]), data={
                'seqfile': f,
                'target': 'current',
            })
        self.assertEqual(obj.bioentry_set.count(), 2)
        self.assertRedirects(response, reverse('object_detail', args=[obj.pk]))
        # Test now with 'any' target
        obj1 = self._create_object(self.spec_type, values=(('ACCENUMB', '1'),))
        obj3 = self._create_object(self.spec_type, values=(('ACCENUMB', '3'),))
        with open(os.path.join(test_file_dir, "multiple2.fasta"), 'r') as f:
            response = self.client.post(reverse('import_seq_file', args=[obj.pk]), data={
                'seqfile': f,
                'target': 'any',
            })
        self.assertContains(response, "There are no accessions with accession number &#x27;6&#x27;")
        obj6 = self._create_object(self.spec_type, values=(('ACCENUMB', '6'),))
        with open(os.path.join(test_file_dir, "multiple2.fasta"), 'r') as f:
            response = self.client.post(reverse('import_seq_file', args=[obj.pk]), data={
                'seqfile': f,
                'target': 'any',
            })
        self.assertRedirects(response, reverse('object_detail', args=[obj.pk]))
        for obj in (obj1, obj3, obj6):
            self.assertEqual(obj.bioentry_set.count(), 1)

    def test_import_multiple_custom_records(self):
        """Custom txt file import linked to multiple accessions."""
        objs = [
            self._create_object(self.spec_type, values=(('ACCENUMB', '581'),)),
            self._create_object(self.spec_type, values=(('ACCENUMB', '586'),)),
            self._create_object(self.spec_type, values=(('ACCENUMB', '588'),)),
        ]
        self.client.login(username=self.user.username, password='johnpw')
        with open(os.path.join(test_file_dir, "multiple_custom.txt"), 'r') as f:
            response = self.client.post(reverse('import_seq_file', args=[objs[0].pk]), data={
                'seqfile': f,
                'target': 'any',
            })
        for obj in objs:
            self.assertEqual(obj.bioentry_set.count(), 1)

    def test_getting_seq_record(self):
        """ Get and check SeqRecord """
        seqreq = self.db.lookup(name="Z78533")
        self.assertEqual(seqreq.id, "Z78533.1")
        self.assertEqual(seqreq.name, "Z78533")
        self.assertEqual(seqreq.annotations['date'], u'30-NOV-2006')
        self.assertEqual(seqreq.annotations['source'], u"Cypripedium irapeanum")
        # TODO: add comments in sample file and test it

        # Also check Seq
        self.assertEqual(str(seqreq.seq[:5]), "CGTAA")
        self.assertEqual(str(seqreq.seq.complement()[:5]), "GCATT")

    def test_display_seq_record(self):
        seq = self.db.bioentry_set.all()[0]
        obj = BaseObject.objects.create(title=u"Test spécimen", obj_type=self.spec_type,
            owner=self.user, status="public")
        seq.baseobjects.add(obj)
        response = self.client.get(reverse('display_sequence', args=[obj.id, seq.pk]))
        self.assertContains(response, "Test sp")
