"""
Custom script to load sequences from a file structured like:
#
M_<accenumb> <description>
<sequence>

M_...
#
"""
from io import StringIO
from invent.models import BaseObject
from Bio import SeqIO
from bioseq.models import BioDatabase

read_seqs = []

with open('./BLAST_seq_juin2017.txt', 'r') as fh:
    while True:
        descr = fh.readline()
        seq = fh.readline()
        fh.readline()  # blank
        if descr == '' and seq == '':
            break
        read_seqs.append((descr.strip(), seq.strip()))

db = BioDatabase.objects.get(name='main')

for descr, seq in read_seqs:
    accenum, descr2 = descr.split(None, 1)
    accenum = accenum.replace('M_', '')
    accs = BaseObject.get_by_attr_value('ACCENUMB', accenum)
    if len(accs) != 1:
        print("For %s value, Found: %s" % (accenum, accs))
        continue
    acc = accs[0]
    fasta = StringIO(u'>%s %s\n%s' % (accenum, descr2, seq))
    for rec in SeqIO.parse(fasta, 'fasta'):
        rec.base_object = acc
        db.load([rec])
    print("Sequence imported for accession '%s'" % acc)
