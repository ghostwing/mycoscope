import os

from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase, override_settings

from invent.models import (Attribute, AttributeForObject, BaseObject, ObjectType,
    Vocabulary, VocabValue)


def test_in_english(fun):
    """ Decorator to force using English as current language for a test """
    def _decorator(self):
        old_LANGUAGES = settings.LANGUAGES
        old_LANGUAGE_CODE = settings.LANGUAGE_CODE
        settings.LANGUAGES = (('en', 'English'),)
        settings.LANGUAGE_CODE = 'en'
        fun(self)
        settings.LANGUAGES = old_LANGUAGES
        settings.LANGUAGE_CODE = old_LANGUAGE_CODE
        return

    return _decorator


@override_settings(PASSWORD_HASHERS=['django.contrib.auth.hashers.MD5PasswordHasher'])
class BaseTestCase(TestCase):
    def setUp(self):
        if 'policy.middleware.PolicyMiddleware' in settings.MIDDLEWARE:
            settings.MIDDLEWARE.remove('policy.middleware.PolicyMiddleware')
        self.user = User.objects.create_user('john', 'doe@example.org', 'johnpw')

        self.img_type = ObjectType.objects.create(code="image", tname="Image")
        self.spec_type = ObjectType.objects.create(code="specimen", tname="Specimen", searchable=True)
        self.spec_type.can_contain.set([self.img_type])
        self.list_type = ObjectType.objects.create(code="inventory", tname="Inventory", searchable=True)
        self.list_type.can_contain.set([self.img_type, self.spec_type])

        txt_attr = Attribute.objects.create(name="ATTR_TEXT", datatype="text", atitle="Test attribute")
        int_attr = Attribute.objects.create(name="ATTR_INT", datatype="integer", atitle="Test integer attribute")

        self.voc_yesno = Vocabulary.objects.create(name="YesNo", ordering='code')
        VocabValue.objects.create(vocab=self.voc_yesno, value='oui', value_code='yes')
        VocabValue.objects.create(vocab=self.voc_yesno, value='non', value_code='no')
        vocab_attr = Attribute.objects.create(
            name="ATTR_VOCAB", datatype="text", atitle="Test vocab attribute",
            vocab=self.voc_yesno
        )
        Attribute.objects.create(name="ATTR_QUERYSET", datatype="qset", atitle="Queryset attribute",
            queryset="BaseObject.objects.filter(obj_type__code='specimen')")
        Attribute.objects.create(name="ATTR_DATE", datatype="date", atitle="Date attribute")
        self.voc_livingdead = Vocabulary.objects.create(name="LivingDead", ordering='code')
        VocabValue.objects.create(vocab=self.voc_livingdead, value='living', value_code='1')
        VocabValue.objects.create(vocab=self.voc_livingdead, value='dead', value_code='0')
        Attribute.objects.create(
            name="ACCSTATUS", datatype="integer", atitle="Specimen status",
            vocab=self.voc_livingdead
        )

        AttributeForObject.objects.create(obj_type=self.spec_type, attr=txt_attr)
        AttributeForObject.objects.create(obj_type=self.spec_type, attr=int_attr)
        AttributeForObject.objects.create(obj_type=self.spec_type, attr=vocab_attr)

        self.inventory = BaseObject.objects.create(
            title="Test inventory",
            obj_type = self.list_type,
            description="Description text.",
            owner= self.user,
            status="public",
        )

    def tearDown(self):
        # Delete uploaded media photo().png/.jpg
        for f in os.listdir("./media/photos"):
            if f.startswith("photo"):
                os.remove("./media/photos/%s" % f)

    def _create_object(self, o_type, values=None, **data):
        """ Create a BasicObject """
        base_data = {
            'title': "Test obj",
            'obj_type': o_type,
            'description': "Description text.",
            'owner': self.user,
            'status': 'public',
        }
        base_data.update(data)
        obj = BaseObject.objects.create(**base_data)
        if values:
            for key, val in values:
                obj.set_value_for(key, val)
        return obj
