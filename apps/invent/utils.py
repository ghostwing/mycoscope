def truncate(text, length):
    if len(text) < length:
        return text
    return "%s…" % text[:length]
