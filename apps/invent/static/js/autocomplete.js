function delay(fn, ms) {
    let timer = 0;
    return function(...args) {
        clearTimeout(timer);
        timer = setTimeout(fn.bind(this, ...args), ms);
    }
}

var Autocomplete = function (options) {
  this.form_selector = options.form_selector;
  this.url = options.url || '/search/autocomplete/';
  this.delay = parseInt(options.delay || 300);
  this.minimum_length = parseInt(options.minimum_length || 3);
  this.form_elem = null;
  this.query_box = null;
};

Autocomplete.prototype.setup = function () {
  const self = this;

  this.form_elem = document.querySelector(this.form_selector);
  this.query_box = this.form_elem.querySelector('input[name=text]');

  // Watch the input box.
  this.query_box.addEventListener('keyup', delay(function () {
    const query = self.query_box.value;

    if (query.length < self.minimum_length) {
      return false;
    }

    self.fetch(query);
  }), 1000);

  // On selecting a result, populate the search field.
  /*this.form_elem.addEventListener('click', function (ev) {
    if (ev.target.classList.contains('ac-result')) {
        self.query_box.value = ev.target.textContent;
        document.querySelector('.ac-results').innerHTML = '';
        return false;
    }
  })*/
};

Autocomplete.prototype.fetch = function (query) {
    const self = this;
    fetch(this.url + '?q=' + encodeURIComponent(query), {method: 'GET'}
    ).then(resp => resp.json()).then(data => {
        self.show_results(data);
    });
};

Autocomplete.prototype.show_results = function (data) {
    // Remove any existing results.
    document.querySelector('.ac-results').innerHTML = '';

    const results = data.results || [];
    const results_wrapper = document.querySelector('.ac-results');
    var base_elem = document.querySelector('#resultTemplate');

    document.querySelector('.ac-head').classList.remove('hidden');
    if (results.length > 0) {
        for (const item of results) {
            const elem = base_elem.content.firstElementChild.cloneNode(true);
            elem.querySelector('.ac-result').textContent = item.title;
            elem.querySelector('.ac-result').href = item.url;
            elem.classList.add('status-' + item.status);
            results_wrapper.append(elem);
        }
        if (data.has_more) {
            let div = document.createElement("div");
            div.append("…");
            results_wrapper.append(div);
        }
    } else {
        let div = document.createElement("div");
        div.append("No results found.");
        results_wrapper.append(div);
    }
};
