var next_col = 3;
$(document).ready(function() {
    if (num_cols <=3) {
        $("#col_next").css('visibility','hidden');
    }
});

function show_next_col() {
    $("#col"+(next_col-3)).hide();
    $("#col"+next_col).show();
    if (next_col == 3) {
        $("#col_prev").css('visibility','visible');
    }
    next_col = next_col + 1;
    if (next_col >= num_cols) {
        $("#col_next").css('visibility','hidden');
    }
    return false;
}
function show_prev_col() {
    $("#col"+(next_col-1)).hide();
    $("#col"+(next_col-4)).show();
    if (next_col == num_cols) {
        $("#col_next").css('visibility','visible');
    }
    next_col = next_col - 1;
    if (next_col == 3) {
        $("#col_prev").css('visibility','hidden');
    }
    return false;
}

function popul_with_search(search_url) {
    // Populate taxon list with search results
    var search_text = $("#search_input").val();
    $.ajax({
        type: "GET",
        url: search_url,
        data: "text="+search_text,
        success: function(data) {
            $('.content_line').html(data);
        },
        error: function (xhr, ajaxOptions, thrownError){
            alert(xhr.responseText);
        }
    });
    return false;
}

function select_and_close(id, name) {
    if (window.opener) {
        window.opener.set_taxon(id, name);
        window.close();
    }
    return false;
}

