/* var T_options, N_options, etc. are defined in search.html template */
function onAttrChange() {
    var filter_num = this.id.split('_',3)[2]; // this.id = id_choice_1
    if (this.value == "") {
        document.getElementById("id_oper_"+filter_num).options.length = 0;
        return;
    }
    var attr_type = this.value.split('_')[0];
    var attr_name = this.value.substring(this.value.indexOf('_')+1);
    populateOper(document.getElementById("id_oper_"+filter_num), eval(attr_type+'_options'));
    if (attr_type == 'V') {
        // Populate Vocab select menu
        $.getJSON(getvocab_url, {'attr_name': attr_name}, function(data) {
            vel = document.getElementById("id_vvalue_"+filter_num);
            vel.options.length = 0;
            $.each(data, function() {vel.options[vel.options.length] = new Option(this.name, this.id);});
        });
        $("#id_value_"+filter_num).hide();
        $("#id_value_"+filter_num).val('');
        $("#id_vvalue_"+filter_num).show();
    } else if (attr_type == 'B') {
        vel = document.getElementById("id_vvalue_"+filter_num);
        vel.options.length = 0;
        vel.options[vel.options.length] = new Option("Vrai", "1");
        vel.options[vel.options.length] = new Option("Faux", "0");
        $("#id_value_"+filter_num).hide();
        $("#id_value_"+filter_num).val('');
        $("#id_vvalue_"+filter_num).show();
    } else {
        $("#id_vvalue_"+filter_num).hide();
        $("#id_value_"+filter_num).show();
    }
}

function populateOper(el, items) {
    el.options.length = 0;
    $.each(items, function () {
        el.options[el.options.length] = new Option(this.name, this.value);
    });
}

/* Add a new search criterion line */
function add_criterion(lk) {
    $(".minus_icon").show();
    // criterion_numb is defined in search.html
    $.ajax({
        type: "GET",
        url: `/search/add_criterion/${criterion_numb}/`,
        data: "",
        success: function(data) {
            lk.parent().before(data);
            criterion_numb += 1;
            $(".attr_select").change(onAttrChange);
        },
        error: function (xhr, ajaxOptions, thrownError){
            alert(xhr.responseText);
        }
    });
    return false;
}

function del_criterion(lk) {
    lk.parent().parent().remove();
    if ($(".minus_icon").length < 2) $(".minus_icon").hide();
    return false;
}

$(document).ready(function() {
  $(".attr_select").change(onAttrChange);
  if ($(".minus_icon").length < 2) $(".minus_icon").hide();
  else $(".minus_icon").show();
});
