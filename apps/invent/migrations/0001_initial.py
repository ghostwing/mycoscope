from django.db import models, migrations
import sorl.thumbnail.fields
import permission.models
import invent.fields
from django.conf import settings
import invent.models


class Migration(migrations.Migration):

    dependencies = [
        ('taxo', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Attribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.SlugField(unique=True, max_length=35)),
                ('active', models.BooleanField(default=True, db_index=True)),
                ('private', models.BooleanField(default=False, help_text='Only visible by users having write access to the target object')),
                ('atitle', models.CharField(max_length=50)),
                ('code', models.CharField(max_length=35, null=True, blank=True)),
                ('ahelp', models.TextField(blank=True)),
                ('datatype', models.CharField(max_length=35, choices=[('speclist', 'List of species'), ('boolean', 'Boolean'), ('meta', 'Metaattribute'), ('integer', 'Integer'), ('date', 'Date'), ('text', 'Text'), ('float', 'Float'), ('qset', 'Queryset')])),
                ('searchable', models.BooleanField(default=True)),
                ('multivalued', models.BooleanField(default=False)),
                ('calculator', models.CharField(max_length=35, null=True, blank=True)),
                ('queryset', models.CharField(max_length=100, null=True, blank=True)),
                ('position_in_group', models.IntegerField(default=0)),
                ('form_template', models.TextField(null=True, blank=True)),
                ('render_template', models.TextField(null=True, blank=True)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'attribute',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AttributeForObject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=15, null=True, blank=True)),
                ('position', models.IntegerField(default=0)),
                ('searchable_in_child', models.BooleanField(default=False)),
                ('attr', models.ForeignKey(blank=True, to='invent.Attribute', null=True, on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['obj_type', 'position'],
                'db_table': 'attribute_for_object',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AttributeGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gname', models.CharField(unique=True, max_length=50)),
                ('category', models.CharField(default='general', max_length=15, choices=[('general', 'General'), ('a0', 'Specimens - A0'), ('list', 'Lists/Inventories'), ('taxo', 'Taxonomy'), ('statistics', 'Statistics')])),
                ('position', models.IntegerField(default=0)),
            ],
            options={
                'db_table': 'attribute_group',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AttributeInGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('position', models.IntegerField(default=0)),
                ('attribute', models.ForeignKey(to='invent.Attribute', on_delete=models.CASCADE)),
                ('group', models.ForeignKey(to='invent.AttributeGroup', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'attribute_in_group',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BaseObject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uuid', invent.fields.UuidField(unique=True, max_length=36, editable=False, blank=True)),
                ('title', models.CharField(max_length=100, verbose_name='Title')),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('status', models.CharField(default='private', max_length=10, choices=[('private', 'Private'), ('public', 'Public')])),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
            ],
            options={
                'db_table': 'base_object',
                'get_latest_by': 'modified',
            },
            bases=(models.Model, permission.models.PermissionMixin),
        ),
        migrations.CreateModel(
            name='FileAttachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fil', models.FileField(upload_to='attachments', verbose_name='File')),
                ('title', models.TextField(verbose_name='Title', blank=True)),
                ('mimetype', models.CharField(max_length=100, editable=False, blank=True)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'fileattachment',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ImageSubobject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', sorl.thumbnail.fields.ImageField(upload_to='photos')),
                ('obj', models.OneToOneField(to='invent.BaseObject', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'image_subobject',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ObjectType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SlugField(max_length=20)),
                ('tname', models.CharField(unique=True, max_length=50)),
                ('icon', models.ImageField(upload_to='icons', blank=True)),
                ('searchable', models.BooleanField(default=False, help_text=b"Available in 'Search in' select on search page")),
                ('can_contain', models.ManyToManyField(to='invent.ObjectType', blank=True)),
            ],
            options={
                'db_table': 'object_type',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ObjectValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value_text', models.TextField(null=True, blank=True)),
                ('value_int', models.IntegerField(blank=True, null=True, validators=[invent.models.int_validator])),
                ('value_float', models.FloatField(null=True, blank=True)),
                ('comments', models.TextField(null=True, blank=True)),
                ('attr', models.ForeignKey(to='invent.Attribute', on_delete=models.CASCADE)),
                ('obj', models.ForeignKey(to='invent.BaseObject', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'object_value',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ObjectValueStats',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value_int', models.IntegerField(blank=True, null=True, validators=[invent.models.int_validator])),
                ('value_float', models.FloatField(null=True, blank=True)),
                ('comments', models.TextField(null=True, blank=True)),
                ('attr', models.ForeignKey(related_name='stat_val', to='invent.Attribute', on_delete=models.CASCADE)),
                ('attr_subj', models.ForeignKey(related_name='target_val', to='invent.Attribute', on_delete=models.CASCADE)),
                ('obj', models.ForeignKey(to='invent.BaseObject', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'object_value_stats',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PhotoAttachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('photo', sorl.thumbnail.fields.ImageField(upload_to='photos')),
                ('title', models.TextField(verbose_name='Title', blank=True)),
                ('order', models.IntegerField(default=0)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'photoattachment',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Vocabulary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=35)),
                ('ordering', models.CharField(default='code', max_length=10, choices=[('code', 'By value code'), ('name', 'By translated name')])),
            ],
            options={
                'db_table': 'vocabulary',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='VocabValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value_code', models.CharField(max_length=10)),
                ('value', models.CharField(max_length=120)),
                ('vocab', models.ForeignKey(to='invent.Vocabulary', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ('value_code',),
                'db_table': 'vocab_value',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='objectvalue',
            name='value_voc',
            field=models.ForeignKey(blank=True, to='invent.VocabValue', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='baseobject',
            name='obj_type',
            field=models.ForeignKey(to='invent.ObjectType', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='baseobject',
            name='owner',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.PROTECT),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='baseobject',
            name='parent',
            field=models.ForeignKey(related_name='children', blank=True, to='invent.BaseObject', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='baseobject',
            name='taxon',
            field=models.ForeignKey(blank=True, to='taxo.Taxon', null=True, on_delete=models.SET_NULL),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='attributeingroup',
            unique_together=set([('attribute', 'group')]),
        ),
        migrations.AddField(
            model_name='attributegroup',
            name='attributes',
            field=models.ManyToManyField(to='invent.Attribute', through='invent.AttributeInGroup'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='attributeforobject',
            name='attr_group',
            field=models.ForeignKey(blank=True, to='invent.AttributeGroup', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='attributeforobject',
            name='obj_type',
            field=models.ForeignKey(to='invent.ObjectType', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='attribute',
            name='group',
            field=models.ForeignKey(blank=True, to='invent.AttributeGroup', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='attribute',
            name='subattributes',
            field=models.ManyToManyField(related_name='subattributes_rel_+', to='invent.Attribute', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='attribute',
            name='vocab',
            field=models.ForeignKey(blank=True, to='invent.Vocabulary', null=True, on_delete=models.PROTECT),
            preserve_default=True,
        ),
        migrations.CreateModel(
            name='ImageObject',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('invent.baseobject',),
        ),
        migrations.CreateModel(
            name='ObjectValueBoolean',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('invent.objectvalue',),
        ),
        migrations.CreateModel(
            name='ObjectValueDate',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('invent.objectvalue',),
        ),
        migrations.CreateModel(
            name='ObjectValueQueryset',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('invent.objectvalue',),
        ),
        migrations.CreateModel(
            name='ObjectValueVocab',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('invent.objectvalue',),
        ),
    ]
