import django.contrib.postgres.search
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0004_remove_point_surface_mgeom'),
    ]

    operations = [
        migrations.AddField(
            model_name='baseobject',
            name='search_vector',
            field=django.contrib.postgres.search.SearchVectorField(null=True),
        ),
    ]
