from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0003_baseobject_weight'),
    ]

    operations = [
        migrations.AlterField(
            model_name='baseobject',
            name='level',
            field=models.PositiveIntegerField(editable=False),
        ),
        migrations.AlterField(
            model_name='baseobject',
            name='lft',
            field=models.PositiveIntegerField(editable=False),
        ),
        migrations.AlterField(
            model_name='baseobject',
            name='rght',
            field=models.PositiveIntegerField(editable=False),
        ),
    ]
