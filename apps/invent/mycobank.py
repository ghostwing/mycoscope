import httpx

from django.conf import settings


class MycobankAPI:
    """
    Example usage:

    import pprint
    from invent.mycobank import MycobankAPI
    
    api = MycobankAPI()
    resp = api.query_by_name("phytophthora")
    pprint.pprint(resp)
    """
    # swagger docs: https://webservices.bio-aware.com/cbsdatabase/index.html?urls.primaryName=BioloMICS%20services%20documentation%20v2
    base_url = 'https://webservices.bio-aware.com/cbsdatabase/'
    websiteid = 85
    table_view = "Mycobank WS"  # or Specimens WS, etc.

    def get_token(self, client=None):
        client = client or httpx
        client_id = settings.MYCOBANK_CLIENTID
        client_secret = settings.MYCOBANK_CLIENTID
        r = client.post(f'{self.base_url}connect/token', data={
            'client_id': client_id, 'client_secret': client_secret, 'grant_type': 'client_credentials',
        })
        token = r.json()['access_token']

    def query_by_name(self, name):
        with httpx.Client() as client:
            token = self.get_token(client)
            url = f'{base_url}v2/search/{self.table_view}/findByName?name={name}'
            r = client.get(url, headers={
                "Authorization": f"Bearer {token}", "websiteId": str(self.websiteid),
            })
            return r.json()
