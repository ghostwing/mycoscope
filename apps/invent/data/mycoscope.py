# -*- coding: utf-8 -*-

mycoscope_attributes = (
    # Host plant
    {'name':"HOSTDESCR", 'title':{'en':"Host plant/organism", 'fr':u"Plante/organisme hôte"},
     'code':"MYCO-01", 'help':u"Nom de la plante ou de l'organisme hôte", 'datatype':"text", 'vocab':None},
    {'name':"HOSTORGAN", 'title':{'en':"Host plant organ", 'fr':u"Organe de la plante hôte"},
     'code':"MYCO-02", 'help':u"fleur, racines, feuilles, tubercule, tiges, collet, via poire, terre, ...", 'datatype':"text", 'vocab':None},
    {'name':"OTHER_SUBSTRATE", 'title':{'en':"Other substrate", 'fr':u"Autre substrat"},
     'code':"MYCO-11", 'help':"", 'datatype':"text", 'vocab':None},
    # Various numbers
    {'name':"ISOLNAME", 'title':{'en':"Isolation name", 'fr':"Nom d'isolement"},
     'code':"MYCO-04", 'help':"", 'datatype':"text", 'vocab':None},
    {'name':"BOXNUMB", 'title':{'en':"Box number", 'fr':u"Numéro de caisse"},
     'code':"MYCO-05", 'help':"", 'datatype':"integer", 'vocab':None},
    # Other attributes
    {'name':"COLLAUTHOR", 'title':{'en':"Collecting person", 'fr':u"Collecteur (personne)"},
     'code':"MYCO-06", 'help':"", 'datatype':"text", 'vocab':None},
    {'name':"CONSERVATION_MILIEU", 'title':{'en':"Culture medium", 'fr':u"Milieu de conservation"},
     'code':"MYCO-07", 'help':"", 'datatype':"text", 'vocab':None},
    {'name':"CONSERVATION_DESCR", 'title':{'en':"Storage", 'fr':u"Conservation"},
     'code':"MYCO-08", 'help':"", 'datatype':"text", 'vocab':None},
    {'name':"LAST_USE", 'title':{'en':"Last use", 'fr':u"Dernière utilisation"},
     'code':"MYCO-09", 'help':"", 'datatype':"text", 'vocab':None},
    {'name':"COUNTRY", 'title':{'en':"Country", 'fr':"Pays"},
     'code':"MYCO-10", 'help':"", 'datatype':"text", 'vocab':"Countries"},
    {'name':"ITS_SEQUENCE", 'title':{'en':"ITS sequence", 'fr':u"Séquence ITS"},
     'code':"MYCO-11", 'help':"", 'datatype':"text", 'vocab':None},
    {'name':"ROOMTEMP_STORAGE", 'title':{'en':"Room temperature storage", 'fr':u"Conservation à température ambiante"},
     'code':"MYCO-12", 'help':"Specifies if specimen should be conserved at room temperature", 'datatype':"text", 'vocab':'YesNo'},
    {'name':"NCBI_CODE", 'title':{'en':"NCBI Code", 'fr':u"Code NCBI"},
     'code':"MYCO-13", 'help':"", 'datatype':"text", 'vocab':None},
)

mycoscope_groups = (
    {'name': "Ecology", 'category': "a0",
     'attr_names':('HOSTDESCR','HOSTORGAN','OTHER_SUBSTRATE') },
    {'name': "Numbers", 'category': "a0",
     'attr_names':('ISOLNAME','BOXNUMB') },
    {'name': "Storage", 'category': "a0",
     'attr_names':('CONSERVATION_MILIEU','CONSERVATION_DESCR','ROOMTEMP_STORAGE') },
)
