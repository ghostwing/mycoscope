# -*- coding: utf-8 -*-

various_attributes = (
    # Vocabularies: -
    {'name':"REGNUM", 'title':{'en':"Kingdom", 'de':u"Reich", 'fr':u"Règne"}, 'code':None, 'help': "", 'datatype':"text", 'vocab':None},
    {'name':"PHYLUM", 'title':{'en':"Phylum", 'de':u"Stamm", 'fr':u"Division"}, 'code':None, 'help': "", 'datatype':"text", 'vocab':None},
    {'name':"CLASSIS", 'title':{'en':"Class", 'de':u"Klasse", 'fr':u"Classe"}, 'code':None, 'help': "", 'datatype':"text", 'vocab':None},
    {'name':"ORDO", 'title':{'en':"Order", 'de':u"Ordnung", 'fr':u"Ordre"}, 'code':None, 'help': "", 'datatype':"text", 'vocab':None},

    {'name':"GENUS_AUTHOR", 'title':{'en':"Gender author", 'de':u"Autor Gattung", 'fr':u"Auteur du genre"}, 'code':None, 'help': "", 'datatype':"text", 'vocab':None},
    {'name':"SISF_ID", 'title':"SISF Number", 'code':None, 'help':"ID number from Synonymic Index Swiss Flora. See www.crsf.ch", 'datatype':"integer", 'vocab':None},
    {'name':"CONTACTNAME", 'title':"Name of contact person", 'code':None, 'help':"Name of the person to contact for further information about this object.", 'datatype':"text", 'vocab':None},
    {'name':"CONTACTEMAIL", 'title':"Email of contact person", 'code':None, 'help':"Email of the person to contact for further information about this object.", 'datatype':"text", 'vocab':None},
    {'name':"NUMBSPEC", 'title':"Number of different species", 'code':None, 'help':"Number of different species contained in the inventory", 'datatype':"integer", 'vocab':None, 'calculator': "get_numbspec"},
    {'name':"REMARKS_TAXO", 'title':"Taxonomy remarks", 'code':None, 'help':"Remarks related to taxonomy", 'datatype':"text", 'vocab':None},
    {'name':"AVAILABILITY", 'title':{'en':"Availability", 'de':u"Verfügbarkeit", 'fr':u"Disponibilité"},
     'code':None, 'help':"", 'datatype':"text", 'vocab': 'YesNo'},
)
