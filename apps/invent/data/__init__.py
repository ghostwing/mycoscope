from .eurisco import eurisco_attributes
from .bdn import bdn_attributes
from .various import various_attributes
# Project attributes :
from .mycoscope import mycoscope_attributes, mycoscope_groups
