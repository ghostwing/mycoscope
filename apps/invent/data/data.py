import itertools

from django.db import IntegrityError

from invent.models import (Vocabulary, VocabValue, Attribute, AttributeGroup,
    AttributeInGroup, ObjectType, AttributeForObject)
from invent.data.vocabularies import vocs

attribute_groups = (
    {'name': "Taxonomy", 'category': "taxo",
     'attr_names':('REGNUM', 'PHYLUM', 'CLASSIS', 'ORDO', 'FAMILY', 'GENUS', 'GENUS_AUTHOR', 'SPECIES', 'SPAUTHOR', 'SUBSPECIES', 'SSPAUTHOR',
                    'SUBTAXA', 'SUBTAUTHOR', 'VARNAME', 'CROPNAME',
                    'VARCOMMONNAMED', 'VARCOMMONNAMEF', 'VARCOMMONNAMEI', 'VARCOMMONNAMER', 'SISF_ID', 'REMARKS_TAXO') },
    {'name': "Location", 'category': "general",
     'attr_names':('COORDINATES','ELEVATION','ORIGCTY','CANTON','MUNICIPALITY','FIELDNAME') },
    {'name': "Environment", 'category': "a0",
     'attr_names':('SAMPSTAT',) },
    {'name': "Origin", 'category': "a0",
     'attr_names':('COLLSRC',) },
    {'name': "Contact", 'category': "general",
     'attr_names':('CONTACTNAME', 'CONTACTEMAIL') },
    {'name': "Statistics", 'category': "list",
     'attr_names':('NUMBSPEC',) },
)

taxon_sources = (
    {'name': "SISF", 'retr_url': "http://www.crsf.ch/index.php?page=espece_carteatlas&no_isfs=%s", 'logo': "img/logos/crsf.jpg"},
    {'name': "GBIF", 'retr_url': "http://data.gbif.org/species/%s/", 'logo': "img/logos/gbif.gif"},
)

def load_vocs(voc_list=None):
    if voc_list is None:
        voc_list = vocs
    for voc in voc_list:
        _, created = Vocabulary.objects.get_or_create(
            name=voc['name'], defaults={'ordering': voc.get('ordering', 'code')}
        )
        if created:
            for key, value in voc['value_dict'].items():
                vocval = VocabValue(vocab=v, value_code=key)
                if isinstance(value, dict):
                    # A dict containing translations with keys being language codes
                    vocval.value = value['en']
                else:
                    vocval.value = value
                vocval.save()
            print("Vocabulary '%s' created." % v.name)

def get_voc(voc_name):
    return Vocabulary.objects.get(name=voc_name)

def load_attrs(*attr_lists):
    """ Create attributes in database.
        Currently only handles new attributes, not modified ones"""
    for d in itertools.chain(*attr_lists):
        attr, created = Attribute.objects.get_or_create(name=d['name'])
        # Support title/code/help change
        if isinstance(d['title'], dict):
            attr.atitle = d['title']['en']
            for key, val in d['title'].items():
                if hasattr(attr, 'atitle_%s' % key):
                    setattr(attr, 'atitle_%s' % key, val)
        else:
            attr.atitle = d['title']
        attr.code = d['code']
        if d.get('help', None):
            if isinstance(d['help'], dict):
                attr.ahelp = d['help']['en']
                for key, val in d['help'].items():
                    if hasattr(attr, 'ahelp_%s' % key):
                        setattr(attr, 'ahelp_%s' % key, val)
            else:
                attr.ahelp = d['help']
        if created:
            for name in ('datatype', 'calculator', 'queryset'):
                setattr(attr, name, d.get(name, None))
            if d.get('vocab', None):
                attr.vocab = get_voc(d['vocab'])
            attr.save()
            if 'subattributes' in d:
                for sd in d['subattributes'].split(","):
                    subattr = Attribute.objects.get(name=sd)
                    attr.subattributes.add(subattr)
            attr.render_template = d.get('render_template', None)
            attr.form_template = d.get('form_template', None)
            attr.save()
            print("New attribute '%s' added in the database" % d['name'])
        else:
            attr.save()
            print("Attribute '%s' updated" % d['name'])
    load_groups()

def load_groups(groups = attribute_groups, update=False):
    # Attribute groups
    for gr in groups:
        group, created = AttributeGroup.objects.get_or_create(gname=gr['name'])
        if created:
            group.category = gr['category']
            group.save()
            for i, attr_name in enumerate(gr['attr_names']):
                try:
                    attr = Attribute.objects.get(name=attr_name)
                except Attribute.DoesNotExist:
                    continue
                try:
                    AttributeInGroup.objects.create(attribute=attr, group=group, position=i)
                except IntegrityError:
                    print("Unable to add attribute %s to group %s. Already done?" % (
                        attr.name, group.gname))
            print("New attribute group '%s' added in the database" % gr['name'])
        elif update:
            if group.category != gr['category']:
                old_cat = group.category
                group.category = gr['category']
                group.save()
                print("Updated Category for group '%s', from '%s' to '%s'" % (group.gname, old_cat, gr['category']))
            current_attr_names = [a.name for a in group.get_attributes()]
            new_attr_names = list(gr['attr_names'])
            for attr_name in gr['attr_names']:
                if attr_name in current_attr_names:
                    current_attr_names.remove(attr_name)
                    new_attr_names.remove(attr_name)
            # Remove attributes no more in group
            for attr_name in current_attr_names:
                attr = Attribute.objects.get(name=attr_name)
                AttributeInGroup.objects.get(attribute=attr, group=group).delete()
                print("Removed attribute '%s' from group '%s'" % (attr_name, group.gname))
            # Link new attributes
            for attr_name in new_attr_names:
                attr = Attribute.objects.get(name=attr_name)
                AttributeInGroup.objects.create(attribute=attr, group=group, position=gr['attr_names'].index(attr_name))
                print("Added attribute '%s' to group '%s'" % (attr_name, group.gname))

def load_object_types(types=None):
    if types is None:
        types = (
            {'code': "image",  'tname': "Image", 'searchable': False,
             'attribute_groups': [], 'can_contain': []},
            {'code': "specimen",  'tname': "Specimen", 'searchable': True,
             'attribute_groups': [gr.gname for gr in AttributeGroup.objects.exclude(category="list")],
             'can_contain': ['image']},
            {'code': "inventory", 'tname': "Inventory", 'icon': 'icons/stock_folder.png', 'attributes': [],
             'searchable': True,
             'attribute_groups': ("Location", "Contact"), 'can_contain': ['specimen', 'inventory', 'image']},
        )
    for type_d in types:
        attributes = type_d.pop('attributes', [])
        attribute_groups = type_d.pop('attribute_groups', [])
        can_contain = type_d.pop('can_contain', [])
        containable_by = type_d.pop('containable_by', [])
        typ, created = ObjectType.objects.get_or_create(
            code = type_d['code'],
            defaults = type_d)
        if created:
            for idx, attr_name in enumerate(attributes):
                try:
                    attr = Attribute.objects.get(name=attr_name)
                except Attribute.DoesNotExist:
                    print("Unable to find attribute %s to link with object type %s" % (attr_name, typ.code))
                    continue
                link = AttributeForObject.objects.create(obj_type=typ, attr=attr, position=idx*2)
            for idx, gr_name in enumerate(attribute_groups):
                try:
                    group = AttributeGroup.objects.get(gname=gr_name)
                except AttributeGroup.DoesNotExist:
                    print("Unable to find attribute group %s to link with object type %s" % (gr_name, typ.code))
                    continue
                attrs = AttributeForObject.objects.create(obj_type=typ, attr_group=group, position=idx*2)
            #if hasattr(inv, 'tname_en'): inv.name_en = "Inventory"
            #if hasattr(inv, 'tname_fr'): inv.name_fr = "Inventaire"
            #if hasattr(inv, 'tname_de'): inv.name_de = "Aufstellung"
            #inv.save()
            for code in can_contain:
                other_type = ObjectType.objects.get(code=code)
                typ.can_contain.add(other_type)
            for code in containable_by:
                other_type = ObjectType.objects.get(code=code)
                other_type.can_contain.add(typ)
    print("Object types %s loaded" % ",".join([t['code'] for t in types]))
