import os
import re
import types
from collections import OrderedDict

from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.postgres.search import SearchVector, SearchVectorField
from django.contrib.staticfiles import finders
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db import models
from django.db.models import Q, Value
from django.db.models.query import QuerySet
from django.db.models.signals import post_save
from django.template import Template
from django.urls import reverse
from django.utils.encoding import force_str
from django.utils.safestring import mark_safe, SafeData
from django.utils.translation import gettext, gettext_lazy as _

from sorl.thumbnail import ImageField
from mptt.managers import TreeManager
from mptt.models import MPTTModel

from invent.fields import UuidField
from invent.utils import truncate
from taxo.models import Taxon
from permission.models import Permission, PermissionMixin

# Monkey-patching User
class UserFunctions:
    def get_inventories(self):
        # Maybe return all root objects ?
        return self.baseobject_set.filter(obj_type__code="inventory").order_by('title')

    def is_siteadmin(self):
        """ Siteadmin can access 'user-oriented' site administration (not Django backend) """
        return self.is_superuser or \
               set([g.name for g in self.groups.all()]) & set(settings.SITE_ADMIN_GROUPS)

User.__bases__ += (UserFunctions,)

class DateValidator(object):
    code = 'invalid'
    valid_eurisco = r'^(?P<year>[12]\d\d\d)((?P<month>[-01][-0-9])(?P<day>[-0-3][-0-9]))?$' # 2004 or 2008----
    valid_iso     = r'^(?P<year>[12]\d\d\d)-(?P<month>[01]\d)-(?P<day>[0-3]\d)$' # 2008-03-12
    valid_ch_fr   = r'^((?P<day>[0-3]?\d)[\./])?(?P<month>[01]?\d)[\./](?P<year>[12]\d\d\d)$' # 21/12/2006 ou 04.07.2005

    def __call__(self, value):
        if not isinstance(value, str):
            value = str(value)
        m = re.match(self.valid_eurisco, value) or re.match(self.valid_iso, value) or re.match(self.valid_ch_fr, value)
        if m is None:
            raise ValidationError("The value '%s' is not valid for data type Date" % (value,), code=self.code)

date_validator = DateValidator()


class ObjectType(models.Model):
    """ Initial object types are filled by fixtures/initial_data.json """
    code        = models.SlugField(max_length=20)
    tname       = models.CharField(max_length=50, unique=True)
    icon        = models.ImageField(upload_to="icons", blank=True)
    searchable  = models.BooleanField(default=False,
        help_text="Available in 'Search in' select on search page")
    can_contain  = models.ManyToManyField('self', symmetrical=False, blank=True)

    class Meta:
        db_table = "object_type"

    def __str__(self):
        return self.tname

    @classmethod
    def as_choices(cls, only_searchable=False):
        """ Return a list of tuples of all object types suitable for a choice form widget """
        if only_searchable:
            return [(o.id, o.tname) for o in cls.objects.filter(searchable=True)]
        else:
            return [(o.id, o.tname) for o in cls.objects.all()]

    def has_taxonomy(self):
        return bool(self.attributeforobject_set.filter(attr_group__category='taxo').count())

    @property
    def possible_attributes(self):
        """ Return an attribute structure in the form (OrderedDict):
            {'group_name': [attributes,], ...}
        """
        if not hasattr(self, '_possible_attributes'):
            groups = [afo.attr_group for afo in self.attributeforobject_set.exclude(attr_group__isnull=True).select_related('attr_group')]
            self._possible_attributes =OrderedDict(zip([g.gname for g in groups], [[] for i in range(len(groups))]))
            for aig in AttributeInGroup.objects.filter(group__in=groups).order_by('position'):
                self._possible_attributes[aig.group.gname].append(aig.attribute)
            # Add non-grouped attributes
            for afo in self.attributeforobject_set.filter(attr_group__isnull=True).order_by('position'):
                self._possible_attributes.setdefault("Other Attributes", []).append(afo.attr)
        return self._possible_attributes

    def get_groups(self, categ_exclude=[], with_not_grouped=False):
        """ Return a list of all groups attached to this object type
            If with_not_grouped is True, a final fake group is added with all attributes for this ObjectType not in an attribute group
        """
        groups = [afo.attr_group for afo in self.attributeforobject_set.select_related('attr_group').all().order_by('position') if afo.attr_group and afo.attr_group.category not in categ_exclude]
        if with_not_grouped:
            groups.append(AttributeForObject.get_others_grouped(self))
        return groups

    def get_attributes(self):
        """ Returns a list of attributes valid for this object type """
        attr_list = []
        for afo in self.attributeforobject_set.all().order_by('position'):
            attr_list.extend(afo.get_attributes())
        for attr in attr_list:
            if attr.datatype == 'meta':
                # Add subattributes for metaattributes
                [attr_list.insert(attr_list.index(attr)+i+1, sub) for i, sub in enumerate(attr.subattributes.all())]
        return attr_list


class BaseObjectManager(TreeManager):
    def filter_writable_for_user(self, user):
        if user.is_superuser:
            return self.get_queryset()
        else:
            group_pks = [g.pk for g in user.groups.all()]
            return self.get_queryset().filter(
                Q(permissions__group__pk__in=group_pks, permissions__perm='rw') |
                Q(status="private", owner__pk=user.pk)
            )

STATUS_CHOICES = (
    ('private', _("Private")),
    ('public', _("Public")),
)
class BaseObject(MPTTModel, PermissionMixin):
    obj_type = models.ForeignKey(ObjectType, on_delete=models.CASCADE)
    uuid        = UuidField(unique=True, auto=True)
    title       = models.CharField(max_length=100, verbose_name=_("Title"))
    description = models.TextField(blank=True, null=True, verbose_name=_("Description"))
    parent = models.ForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    taxon = models.ForeignKey(Taxon, blank=True, null=True, on_delete=models.SET_NULL)
    owner = models.ForeignKey(User, on_delete=models.PROTECT)
    status      = models.CharField(max_length=10, choices=STATUS_CHOICES, default='private')
    created     = models.DateTimeField(auto_now_add=True)
    modified    = models.DateTimeField(auto_now=True)
    weight      = models.SmallIntegerField(
        default=0, help_text="Higher numbers will make the item appear first in some lists")
    # Denormalized search content
    search_vector = SearchVectorField(null=True)
    photos      = GenericRelation('PhotoAttachment')
    files       = GenericRelation('FileAttachment')
    permissions = GenericRelation(Permission)
    
    objects = tree = BaseObjectManager()

    class Meta:
        db_table = "base_object"
        get_latest_by = "modified"

    def __str__(self):
        return self.title or str(self.pk)

    def __len__(self):
        return len(self.children.all())

    def __bool__(self):
        return True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._cached_values = None

    def get_absolute_url(self):
        return reverse('object_detail', args=[str(self.id)])
    
    # Functions inherited from mptt:
    #get_ancestors, get_children, get_descendants...
    #https://django-mptt.readthedocs.io/

    def _fill_cache(self):
        if self._cached_values is None:
            values = self.get_values()
            self._cached_values = {}
            for val in values:
                if val.attr.name in self._cached_values:
                    if isinstance(self._cached_values[val.attr.name], MetaValue):
                        self._cached_values[val.attr.name].append_value(val)
                    else:
                        meta = MetaValue(val.attr, val.obj, [self._cached_values[val.attr.name], val])
                        self._cached_values[val.attr.name] = meta
                else:
                    self._cached_values[val.attr.name] = val

    def index(self):
        self.search_vector = SearchVector('title') + SearchVector('description')
        for val in self.get_values():
            text = force_str(val.get_value())
            self.search_vector += SearchVector(Value(text, output_field=models.TextField()))
        if self.obj_type.code == 'image':
            self.search_vector += SearchVector(Value(self.parent.title, output_field=models.TextField()))

    def save(self, **kwargs):
        is_new = not bool(self.pk)
        if not is_new:
            self.index()
        super().save(**kwargs)
        if is_new:
            self.index()
            super().save(update_fields=['search_vector'])

    @classmethod
    def get_by_attr_value(cls, attr_name, value, limit_to=None):
        attr = Attribute.objects.get(name=attr_name)
        query_dict = {
            'attr__name': attr_name,
            attr.get_value_field(): value,
        }
        if limit_to is not None:
            query_dict['obj__obj_type__code'] = limit_to
        vals = ObjectValue.objects.filter(**query_dict).values_list('obj_id', flat=True).distinct()
        return cls.objects.filter(pk__in=vals)

    def can_edit(self, user):
        """ Return True if user can edit this object """
        return self.has_write_permission(user)

    def can_view(self, user):
        return self.has_read_permission(user)

    def is_containable(self):
        return bool(self.obj_type.can_contain.exclude(code='image').count())

    def is_biosequenceable(self):
        return 'bioseq' in settings.INSTALLED_APPS and self.obj_type.code in ('specimen',)

    def breadcrumb(self, link_self=False):
        """ Produce a tuple list structure to give to template to build the breadcrumb """
        bc_list = [(_("Home"), "/")]
        bc_list.extend([(str(o), o.get_absolute_url()) for o in self.get_ancestors()])
        bc_list.append((self.get_title(), link_self and self.get_absolute_url() or ""))
        return bc_list

    def get_title(self):
        return self.title or str(self.pk)

    def get_features(self):
        """ Test if object has images/biosequence and return a list of images as tuples (image url, title)
            Displayed currently in object_list_block (on object_detail) and search results """
        images = []
        if self.get_images().count() > 0:
            images.append((os.path.join(settings.STATIC_URL, 'icons', 'image.png'), gettext("This object has images")))
        if 'bioseq' in settings.INSTALLED_APPS and hasattr(self, 'bioentry_set') and self.bioentry_set.count() > 0:
            images.append((os.path.join(settings.STATIC_URL, 'icons', 'dna-gr.png'), gettext("This object has molecular sequences")))
        return images

    def get_images(self):
        return self.get_children().filter(obj_type__code='image')

    def get_children_by_type(self):
        """ Return a dict of tuples : (count, lists of children), grouped by type """
        grouped_objects = {}
        # get_children returns an empty query self is a leaf node! (and annotate cannot be used then)
        if not self.is_leaf_node():
            obj_types = self.get_children().values('obj_type').annotate(ocount=models.Count('obj_type')).order_by()
            for ot in obj_types:
                grouped_objects[ObjectType.objects.get(id=ot['obj_type'])] = (
                    ot['ocount'],
                    self.get_children().filter(obj_type__pk=ot['obj_type']).order_by('title')
                )
        return grouped_objects

    def get_reverse_objects(self):
        obj_ids = ObjectValue.objects.filter(attr__queryset__contains=self.obj_type.code, value_int=self.pk).values_list('obj_id', flat=True)
        return BaseObject.objects.filter(pk__in=obj_ids).select_related('parent').order_by('title')

    def uuid_url(self):
        return "https://mycoscope.ch%s" % reverse('object_by_uuid', args=[self.uuid])

    def get_icon_url(self):
        """ Return an URL suitable for a src HTML attribute, return default one if no icon property """
        if self.obj_type.icon:
            return self.obj_type.icon.url
        return os.path.join(settings.STATIC_URL, 'icons','default.png')

    def get_attr_dict(self, attr_list, empty_value=''):
        """ Return all values of an object in a OrderedDict with each key corresponding in attr_list.name:
            {'DONORDESCR': 'Description', 'DONORNUMB': 135, ...}
        """
        attr_dict = OrderedDict()
        self._fill_cache()
        remaining_values = self._cached_values.copy()
        for attr in attr_list:
            attr_dict[attr.name] = self.get_value_for(attr.name) or empty_value
            if attr.name in remaining_values:
                del remaining_values[attr.name]
        for k, v in remaining_values.items():
            remaining_values[k] = v.get_value() or empty_value
        return attr_dict, remaining_values

    def get_children_existing_attrs(self):
        """ Return all existing attributes for all children, except 'automatic' ones (like taxo, etc.) """
        return list(Attribute.objects.filter(objectvalue__obj__parent=self).distinct()) #.order_by('name'))

    def get_values(self):
        """ Return a list of values for this object (as ObjectValue objects) """
        if not self.pk:
            return []
        # Add first fake taxo attributes (if any)
        values = ObjectValueTaxo.taxo_values(self)
        values.extend(ObjectValue.objects.filter(obj=self).select_related('obj','attr').as_list())
        return values

    def get_value_for(self, attr_name, as_object=False):
        """ Return the value corresponding to attr_name for this object, either as string or object """
        if self._cached_values is None:
            try:
                val_obj = ObjectValue.objects.get(obj=self, attr__name=attr_name)
            except ObjectValue.DoesNotExist:
                val_obj = None
        else:
            val_obj = self._cached_values.get(attr_name, None)
        if as_object:
            return val_obj
        else:
            return val_obj and val_obj.get_value() or None

    def set_value_for(self, attr_name, value, index=True):
        """ Set an attribute value for this object """
        if value is None or value == '':
            # Do not set empty values
            return
        attr = Attribute.objects.get(name=attr_name)
        val_obj, cr = ObjectValue.objects.get_or_create(obj=self, attr=attr)
        val_obj.set_value({'value_%s' % attr_name: value})
        if index:
            self.save()

    def set_statvalue_for(self, stat_attr, target_attr, value):
        if value is None or value == '':
            # Do not set empty values
            return
        val_obj = ObjectValueStats.objects.create(obj=self, attr=stat_attr, attr_subj=target_attr)
        val_obj.set_value(value)

    def get_grouped_values(self, user, with_taxo=True):
        """ Return all values grouped by attribute group:
            {'group_name1': {'attr_title1': (attr1_obj, [Value1]), 'attr_title2': (attr2_obj, [Value2, Value3]), ...},
             'group_name2': {...}, ...}
            If a group has a 'renderer' key, its value is used as a template to render the group values
        """
        self._fill_cache()

        val_groups = OrderedDict(zip(self.obj_type.possible_attributes.keys(),
            [OrderedDict() for i in range(len(self.obj_type.possible_attributes.keys()))]))
        can_view_private = self.has_permission(('r+', 'rw'), user)
        for grp, attrs in self.obj_type.possible_attributes.items():
            for attr in attrs:
                if attr.name in self._cached_values:
                    val = self._cached_values[attr.name]
                    if not with_taxo and isinstance(val, ObjectValueTaxo):
                        continue
                    if val.attr.private and not can_view_private:
                        continue

                    #val_groups.setdefault(grp, OrderedDict())
                    if attr.atitle not in val_groups[grp]:
                        val_groups[grp][attr.atitle] = (attr, [])
                    val_groups[grp][attr.atitle][1].append(val)
        for key in list(val_groups.keys()):
            if not val_groups[key]:
                del val_groups[key]
        if hasattr(self, 'bioentry_set'):
            for bioentry in self.bioentry_set.all():
                val_groups.setdefault(gettext("Molecular Data"), OrderedDict())
                val_groups[gettext("Molecular Data")]['renderer'] = "bioseq/attribute_render.html"
                val_groups[gettext("Molecular Data")][bioentry.identifier] = bioentry
        if len(val_groups) == 1 and list(val_groups.keys())[0] == "Other Attributes":
            val_groups["Attributes"] = val_groups["Other Attributes"]
            del val_groups["Other Attributes"]
        return val_groups

    def get_taxo_value(self, rank_name):
        """ Returns the taxon string value for this object, for rank """
        taxo = self.taxon and self.taxon.get_taxon_by_rank(rank_name) or None
        return taxo and taxo.name or None

    def get_specie(self):
        """ Return the taxon object corresponding to the specie or subspecie, or None if not available """
        sp = None
        if self.taxon:
            sp = self.taxon.get_taxon_by_rank('subspecies') or self.taxon.get_taxon_by_rank('species')
        return sp

    def taxo_bind(self, data):
        """ Link this object to the taxonomy defined by attributes from the 'data' dict """
        try:
            self.taxon = Taxon.get_from_data(data, settings.CREATE_TAXONOMY_ON_IMPORT)
        except ValueError:
            # Put taxo data in REMARKS_TAXO so as to keep them
            rems = ", ".join(["%s: %s" % (key, val) for key,val in data.items() if val])
            existing_rems = self.get_value_for("REMARKS_TAXO")
            if existing_rems:
                rems = "%s, %s" % (existing_rems, rems)
            self.set_value_for("REMARKS_TAXO", rems, index=False)
        self.save()

    # Statistics methods
    def get_numbspec(self):
        """ Return the number of different species/subspecies in the list.
            Method referenced in NUMBSPEC attribute """
        specie_set = set([])
        for obj in self.contents.all():
            sp = obj.get_specie()
            if sp:
                specie_set.add(sp.id)
        return len(specie_set)

class ImageObject(BaseObject):
    class Meta:
        proxy = True


class ImageSubobject(models.Model):
    obj   = models.OneToOneField(BaseObject, on_delete=models.CASCADE)
    image = ImageField(upload_to='photos')
    class Meta:
        db_table = "image_subobject"

    def __str__(self):
        return "Image %s" % self.image.url


# By default, copy permissions of the parent object at creation time
def copy_permissions(sender, instance, created, **kwargs):
    if created and instance.parent:
        for perm in instance.parent.permissions.all():
            p = Permission(content_object=instance, group=perm.group, perm=perm.perm)
            p.save()
post_save.connect(copy_permissions, sender=BaseObject)


class AttributeForObject(models.Model):
    obj_type = models.ForeignKey('ObjectType', on_delete=models.CASCADE)
    attr = models.ForeignKey('Attribute', blank=True, null=True, on_delete=models.CASCADE)
    attr_group = models.ForeignKey('AttributeGroup', blank=True, null=True, on_delete=models.CASCADE)
    # May be used to specify mandatory or optional attributes
    status      = models.CharField(max_length=15, blank=True, null=True)
    position    = models.IntegerField(default=0)
    searchable_in_child = models.BooleanField(default=False)

    class Meta:
        db_table = "attribute_for_object"
        ordering = ['obj_type', 'position']

    def __str__(self):
        if self.attr_id:
            return "Attribute %s for object type '%s'" % (self.attr.name, self.obj_type.code)
        else:
            return "Attribute group %s for object type '%s'" % (self.attr_group.gname, self.obj_type.code)

    @classmethod
    def get_others_grouped(cls, obj_type):
        """ Return a fake attribute group with all not grouped attributes """
        class FakeGroup:
            id       = obj_type.id * -1
            gname    = "Other Attributes"
            category = 'other'
            position = 999
            def get_attributes(self):
                # Return a list of attributes
                return Attribute.objects.filter(
                    attributeforobject__obj_type=obj_type, active=True
                ).order_by('attributeforobject__position')
        return FakeGroup()

    def get_attributes(self):
        """ Return a list of attributes contained in object """
        attr_list = []
        if self.attr:
            attr_list.append(self.attr)
        elif self.attr_group:
            attr_list.extend(self.attr_group.get_attributes())
        return attr_list


class DatatypeChoices(object):
    """ search_tp: T: text, N:number, D: date, V: vocab """
    choices = {
        'text'   : {'name': _("Text"),    'db_vfield': 'value_text',
                    'ffield_class': forms.CharField, 'ffield_opts': {'max_length': 2000},
                    'search_tp': 'T'},
        'integer': {'name': _("Integer"), 'db_vfield': 'value_int',
                    'ffield_class': forms.IntegerField,
                    # MySQL limits on INT
                    'ffield_opts': {'max_value': 2147483647, 'min_value': -2147483647},
                    'search_tp': 'N'},
        'float'  : {'name': _("Float"),   'db_vfield': 'value_float',
                    'ffield_class': forms.FloatField, 'search_tp': 'N'},
        'boolean': {'name': _("Boolean"), 'db_vfield': 'value_int',
                    'ffield_class': forms.BooleanField, 'search_tp': 'B'},
        'date'   : {'name': _("Date"),    'db_vfield': 'value_text',
                    'ffield_class': forms.CharField,
                    'ffield_opts': {'max_length': 10, 'validators': [date_validator]},
                    'search_tp': 'D'},
        'meta'   : {'name': _("Metaattribute"),   'db_vfield': None,   'ffield_class': None},
        'speclist': {'name': _("List of species"), 'db_vfield': 'value_text',
                     'ffield_class': forms.IntegerField},
        'qset'   : {'name': _("Queryset"), 'db_vfield': 'value_int', 'ffield_class': None,
                    'search_tp': 'V'},
    }
    @classmethod
    def as_choices(cls):
        return [(key, ch['name']) for key, ch in cls.choices.items()]
    @classmethod
    def as_searchtp_dict(cls):
        dct = {'vocab': 'V'}
        [dct.setdefault(key, vals['search_tp']) for key, vals in cls.choices.items() if 'search_tp' in vals]
        return dct

dt_choices = DatatypeChoices()

class AttributeManager(models.Manager):
    def used_attributes(self):
        used_ids = Attribute.objects.values_list('id', flat=True).annotate(num_values=models.Count('objectvalue')).filter(num_values__gt=0)
        return self.filter(id__in=list(used_ids))


class Attribute(models.Model):
    name       = models.SlugField(max_length=35, unique=True)
    active     = models.BooleanField(default=True, db_index=True)
    private    = models.BooleanField(default=False,
                                     help_text="Only visible by users having write access to the target object")
    atitle     = models.CharField(max_length=50)
    code       = models.CharField(max_length=35, blank=True, null=True)
    ahelp      = models.TextField(blank=True)
    datatype   = models.CharField(max_length=35, choices=dt_choices.as_choices())
    vocab = models.ForeignKey('Vocabulary', blank=True, null=True, on_delete=models.PROTECT)
    # Define if attribute appears in Search form
    searchable = models.BooleanField(default=True)
    multivalued = models.BooleanField(default=False)
    calculator = models.CharField(max_length=35, blank=True, null=True)
    queryset   = models.CharField(max_length=100, blank=True, null=True)
    # Obsolete field (to delete, replaced by AttributeInGroup)
    group = models.ForeignKey('AttributeGroup', blank=True, null=True, on_delete=models.CASCADE)
    # Obsolete field (to delete, replaced by AttributeInGroup)
    position_in_group = models.IntegerField(default=0)
    subattributes  = models.ManyToManyField('self', blank=True)
    form_template   = models.TextField(blank=True, null=True)
    render_template = models.TextField(blank=True, null=True)
    objects = AttributeManager()

    class Meta:
        db_table = "attribute"
        ordering = ('name',)

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, **kwargs):
        # Put this in model validation as soon as using django 1.2
        need_val_migration = False
        if self.vocab and self.get_value_count() > 0:
            # Check old values are valid in the new vocabulary
            all_values = ObjectValue.objects.filter(attr=self).select_related('value_voc')
            for val in all_values:
                if val.value_voc.vocab != self.vocab:
                    # Validate current value with new vocab
                    self.vocab.valid_value(val.get_value())
                    need_val_migration = True
        super(Attribute, self).save(force_insert, force_update, **kwargs)
        if need_val_migration:
            # Values need to be re-called from DB as old val.attr.vocab seems to be cached
            all_values = ObjectValue.objects.filter(attr=self).select_related('value_voc')
            for val in all_values.as_list():
                val.set_value({'value_%s' % self.name: val.get_value()})

    @classmethod
    def all_attr_names(cls):
        all_names = list(cls.objects.filter(active=True).values_list('name', flat=True))
        # Maybe this should be a real 'special' descriptor ?
        all_names.append('DESCRIPTION')
        return all_names

    def get_datatype(self):
        return self.vocab_id and 'vocab' or self.datatype

    def is_editable(self):
        return not self.calculator

    def get_vocab(self):
        """ Returns the vocabulary as a dictionary list """
        value_list = []
        if self.queryset:
            qs = eval(self.queryset)
            value_list = [{'id':v.id, 'code':None, 'name': str(v)} for v in qs.all()]
        elif self.datatype == 'boolean':
            value_list = [
                {'id':0, 'code':None, 'name': gettext("No")},
                {'id':1, 'code':None, 'name': gettext("Yes")}
            ]
        elif self.vocab:
            qs = self.vocab.vocabvalue_set
            if self.vocab.ordering == 'name':
                qs = qs.order_by('value')
            value_list = [{'id':v.id, 'code':v.value_code, 'name':v.value} for v in qs.all()]
            if self.vocab.name == 'Countries':
                # Add Switzerland as first element in the list
                che_vocab = qs.get(value_code='CHE')
                value_list.insert(0, {'id':che_vocab.id, 'code':che_vocab.value_code, 'name':che_vocab.value})
        return value_list

    def get_value_field(self):
        """ Returns the name of the corresponding object value field """
        if self.vocab_id: return 'value_voc'
        return dt_choices.choices[self.datatype]['db_vfield']

    def get_value_count(self):
        """ Returns the number of values in the database for this attribute """
        return ObjectValue.objects.filter(attr=self).count() if self.pk else 0

    def get_form_field(self, initial=None, with_label=True, required=False):
        """ Return the form field corresponding to this attribute datatype """
        from invent.forms import TaxonSelectWidget
        def force_list(val):
            if val is not None and not isinstance(val, types.ListType):
                val = [val]
            return val

        label = with_label and self.atitle or ""
        if self.vocab:
            choices = [(v['code'],truncate(v['name'],58)) for v in self.get_vocab()]
            if not required:
                choices.insert(0, ('','------'))
            field = forms.ChoiceField(choices=choices, label=label, initial=initial, help_text=self.ahelp, required=required)
        elif self.datatype == 'qset':
            choices = [(v.id, str(v)) for v in eval(self.queryset)]
            if not required:
                choices.insert(0, ('','------'))
            if self.multivalued:
                field = forms.MultipleChoiceField(choices=choices, label=label,
                    initial=force_list(initial), help_text=self.ahelp, required=required)
            else:
                field = forms.ChoiceField(choices=choices, label=label,
                    initial=initial, help_text=self.ahelp, required=required)
        else:
            fieldClass = dt_choices.choices[self.datatype]['ffield_class']
            if fieldClass is None:
                return None
            field_opts = {'label': label, 'initial': initial, 'help_text': self.ahelp, 'required': required}
            field_opts.update(dt_choices.choices[self.datatype].get('ffield_opts', {}))
            field = fieldClass(**field_opts)
            if "\n" in force_str(initial):
                field.widget = forms.Textarea(attrs={'rows':2, 'cols':40})
            if (self.datatype == 'speclist'):
                # cannot be declared at DataChoices level because of circular dependency
                field.widget = TaxonSelectWidget
            if (self.datatype == 'text'):
                field.widget.attrs["style"] = "width:98%;"
        return field

CATEGORY_CHOICES = (
    ('general', _("General")), # Category for all sort of objects
    ('a0',      _("Specimens - A0")),
    ('list',    _("Lists/Inventories")),
    #('list:subcat', _("lists of type 'subcat'")),
    ('taxo',    _("Taxonomy")),
    ('statistics', _("Statistics")),
)
class AttributeGroup(models.Model):
    gname       = models.CharField(max_length=50, unique=True)
    category    = models.CharField(max_length=15, choices=CATEGORY_CHOICES, default='general')
    position    = models.IntegerField(default=0)
    attributes  = models.ManyToManyField(Attribute, through='AttributeInGroup')

    class Meta:
        db_table = "attribute_group"

    def __str__(self):
        return self.gname or str(self.id)
    
    @classmethod
    def get_groups(cls, categ_filter=[]):
        groups = cls.objects.all().order_by('position', 'gname')
        if categ_filter:
            groups = [gr for gr in groups if gr.category in categ_filter]
        return groups

    def get_attributes(self):
        """ Return a list of attributes """
        return [aig.attribute for aig in self.attributeingroup_set.filter(attribute__active=True).order_by('position')]


class AttributeInGroup(models.Model):
    attribute = models.ForeignKey(Attribute, on_delete=models.CASCADE)
    group = models.ForeignKey(AttributeGroup, on_delete=models.CASCADE)
    position = models.IntegerField(default=0)

    class Meta:
        db_table = "attribute_in_group"
        unique_together = ("attribute", "group")

    def __str__(self):
        return "%s in group %s (%d)" % (self.attribute.name, self.group.gname, self.position)


ORDERING_CHOICES = (
    ('code', _("By value code")),
    ('name', _("By translated name")),
)
class Vocabulary(models.Model):
    name     = models.CharField(max_length=35, unique=True)
    ordering = models.CharField(max_length=10, choices=ORDERING_CHOICES, default='code')

    class Meta:
        db_table = "vocabulary"

    def __str__(self):
        return self.name

    def valid_value(self, value):
        """ Check if value is valid for this vocabulary and return the VocabValue if valid
            Raise ValueError otherwise
        """
        value = str(value)
        try:
            vv = VocabValue.objects.get(vocab=self, value_code__iexact=value)
        except VocabValue.DoesNotExist:
            # Also try to get value through value
            vv = VocabValue.objects.filter(vocab=self, value__iexact=value)
            if len(vv) == 1:
                vv = vv[0]
            elif len(vv) == 0:
                raise ValueError(gettext("The value '%(val)s' has not been found in the vocabulary %(voc)s" % {
                    'val': value, 'voc': self.name}))
            else:
                for vv_sup in vv[1:]:
                    if vv_sup != vv[0]:
                        raise ValueError(gettext("Possible ambiguity with value '%(val)s' in vocabulary %(voc)s" % {
                            'val': value, 'voc': self.name}))
                vv = vv[0]
        return vv


class VocabValue(models.Model):
    vocab = models.ForeignKey('Vocabulary', on_delete=models.CASCADE)
    value_code  = models.CharField(max_length=10)
    value       = models.CharField(max_length=120)

    class Meta:
        db_table = "vocab_value"
        ordering = ('value_code',)

    def __str__(self):
        return "%s: %s" % (self.value_code, self.value)


class ObjectValueQuerySet(QuerySet):
    def as_list(self):
        """ Return a list of queryset objects, of the right type
            Should always be the last method called on a queryset """
        olist = []
        for o in self:
            olist.append(o.get_proxied())
        return olist


class ObjectValueManager(models.Manager):
    def get_queryset(self):
        return ObjectValueQuerySet(self.model)

    def get(self, *args, **kwargs):
        return super(ObjectValueManager, self).get(*args, **kwargs).get_proxied()

    def get_or_create(self, **kwargs):
        obj, cr = super(ObjectValueManager, self).get_or_create(**kwargs)
        return obj.get_proxied(), cr

class ObjectValueFactory(object):
    def __new__(cls, *args, **kwargs):
        ov = ObjectValue(*args, **kwargs)
        return ov.get_proxied()

def int_validator(value):
    # MySQL limits on INT
    if value > 2147483647 or value < -2147483647:
        raise ValidationError("value out of limits")


class ObjectValue(models.Model):
    obj = models.ForeignKey('BaseObject', on_delete=models.CASCADE)
    attr = models.ForeignKey('Attribute', on_delete=models.CASCADE)
    value_text  = models.TextField(blank=True, null=True)
    value_int   = models.IntegerField(blank=True, null=True, validators=[int_validator])
    value_float = models.FloatField(blank=True, null=True)
    value_voc = models.ForeignKey(VocabValue, blank=True, null=True, on_delete=models.CASCADE)
    comments    = models.TextField(blank=True, null=True)
    objects = ObjectValueManager()

    class Meta:
        db_table = "object_value"

    def __str__(self):
        return "%s: %s" % (self.attr.name, self.get_value())

    def _value_field(self):
        return self.attr.get_value_field()

    def get_proxied(self):
        """ Set the appropriate ObjectValue proxy class and return self """
        if self.attr and self.attr.datatype == 'date':
            self.__class__ = ObjectValueDate
        if self.attr and self.attr.datatype == 'boolean':
            self.__class__ = ObjectValueBoolean
        elif self.attr and self.attr.vocab:
            self.__class__ = ObjectValueVocab
        elif self.attr and self.attr.queryset:
            self.__class__ = ObjectValueQueryset
        return self

    def clean(self):
        """ Object validation method to prevent several values for same attribute,
            until a proper multivalue logic is implemented """
        if not self.attr.multivalued:
            exist_count = ObjectValue.objects.filter(obj=self.obj, attr=self.attr).exclude(pk=self.id).count()
            if exist_count > 0:
                raise ValidationError(_("There is already a value for this attribute"))

    def validate(self, value):
        if isinstance(value, str):
            return value.strip()
        return value

    def get_value(self, raw=False):
        if self.attr.calculator:
            # Dynamic value
            method = getattr(self.obj, self.attr.calculator, None)
            if callable(method):
                value = method()
            else:
                raise ValueError("")
            return value
        if self.value_voc:
            if raw:
                value = self.value_voc.value_code
            else:
                value = self.value_voc.value
        elif self.attr.datatype == 'qset':
            if raw:
                value = self.value_int
            else:
                base_qset = eval(self.attr.queryset)
                try:
                    value = base_qset.filter(pk=self.value_int)[0]
                    if hasattr(value, 'get_absolute_url'):
                        value = mark_safe('<a href="%s">%s</a>' % (value.get_absolute_url(), value))
                    else:
                        value = str(value)
                except (ObjectDoesNotExist, IndexError):
                    value = gettext("unvalid object")
        else:
            value = getattr(self, self._value_field())
        return value

    def get_subvalues(self, raw=False):
        """ For meta-attributes, return a dict with values of subattributes """
        if not self.attr.datatype == 'meta':
            return None
        subvalues = self.__class__.objects.filter(obj=self.obj, attr__in=self.attr.subattributes.all())
        return dict([(val.attr.name, val.get_value(raw)) for val in subvalues])

    def set_value(self, value_dict):
        """ value_dict is a dict with the same struct as form.cleaned_data, in the form:
            {'value_DESCRNAME1': value, 'value_DESCRNAME2': value, 'comments': ...} """
        assert isinstance(value_dict, dict)
        if not self.attr.is_editable():
            return None
        if self.attr.datatype == 'meta':
            # Set value for each subattribute
            for subd in self.attr.subattributes.all():
                subd_val, cr = self.__class__.objects.get_or_create(attr=subd, obj=self.obj)
                subd_val.set_value(value_dict)
            self.value_int = 1
        elif self.attr.vocab:
            value = value_dict.get('value_%s' % self.attr.name)
            vv = self.validate(value)
            self.value_voc = vv
        else:
            value = self.validate(value_dict.get('value_%s' % self.attr.name))
            setattr(self, self._value_field(), value)
            if self.attr.datatype == 'speclist':
                # Cache taxon name in value_text
                taxon = Taxon.objects.get(pk=value)
                self.value_text = taxon.get_species_name()
        self.comments = value_dict.get('comments', None)
        self.full_clean()
        self.save()
        if self.obj._cached_values and self.attr.name not in self.obj._cached_values:
            self.obj._cached_values[self.attr.name] = self

    def render(self, context):
        """ Prepare the value to be HTML ready """
        if self.attr.render_template:
            t = Template(self.attr.render_template)
            new_context = {'attribute': self.attr}
            for d in self.attr.subattributes.all():
                # Get values from all subattributes and enrich context with these values
                try:
                    subval = self.__class__.objects.get(attr__name=d.name, obj=self.obj)
                except self.__class__.DoesNotExist:
                    continue
                new_context[d.name] = subval.get_value()
            context.update(new_context)
            return t.render(context)
        else:
            WORD_MAX_SIZE = 32
            rendered_val = self.get_value()
            if not isinstance(rendered_val, SafeData):
                rendered_val = force_str(self.get_value())
                for word in rendered_val.split():
                    if len(word) > WORD_MAX_SIZE:
                        # Cut word and replace in val
                        new_word = ""
                        for i in range(0, (len(word) // WORD_MAX_SIZE) + 1):
                            new_word += word[i * WORD_MAX_SIZE: (i+1) * WORD_MAX_SIZE] + " "
                        rendered_val = rendered_val.replace(word, new_word[:-1])
                rendered_val = rendered_val.replace("\n", "<br/>")
            if self.comments:
                rendered_val = "%s <em>(%s)</em>" % (rendered_val, self.comments)
            return rendered_val


class ObjectValueDate(ObjectValue):
    class Meta:
        proxy = True

    def validate(self, value):
        date_validator(value)
        return value

class ObjectValueBoolean(ObjectValue):
    class Meta:
        proxy = True

    def validate(self, value):
        if value in ('1', 'on', 'true', 'True'):
            return True
        elif value in ('0', 'off', 'false', 'False'):
            return False
        return value

    def render(self, context):
        rendered_val = self.get_value() and gettext("Yes") or gettext("No")
        if self.comments:
            rendered_val = "%s <em>(%s)</em>" % (rendered_val, self.comments)
        return rendered_val

class ObjectValueVocab(ObjectValue):
    class Meta:
        proxy = True

    def validate(self, value):
        """ Get a vocab value object corresponding to value (string) """
        return self.attr.vocab.valid_value(value)

class ObjectValueQueryset(ObjectValue):
    class Meta:
        proxy = True

    def validate(self, value):
        """ Get a vocab value object corresponding to value (string) """
        value_pk = None
        if isinstance(value, models.Model):
            value_pk = value.pk
        else:
            try:
                value_pk = int(value)
            except: pass
        if value_pk is None or value_pk not in eval(self.attr.queryset + ".values_list('pk', flat=True)"):
            raise ValidationError("The value '%s' is not valid for attribute %s" % (value, self.attr.name))
        return value_pk


class MetaValue(object):
    """ Abstraction level over ObjectValue ORM model (handle multivalues) """

    EMPTY_VALUE = ['', []]

    def __init__(self, attr, obj, values=None):
        self.id = attr.name
        self.obj = obj
        self.attr = attr
        self.ovalues = []
        if values is not None:
            self.ovalues = values

    @property
    def comments(self):
        return "\n".join(filter(None, [val.comments for val in self.ovalues]))

    def set_value(self, value_dict):
        value = value_dict.get('value_%s' % self.attr.name)
        self.delete()
        if isinstance(value, (list, tuple)):
            value = filter(lambda v: v not in self.EMPTY_VALUE, value)
            for val in value:
                ovalue = ObjectValueFactory(obj=self.obj, attr=self.attr)
                value_dict['value_%s' % self.attr.name] = val
                ovalue.set_value(value_dict)
                self.ovalues.append(ovalue)
        elif value not in self.EMPTY_VALUE:
            ovalue = ObjectValueFactory(obj=self.obj, attr=self.attr)
            ovalue.set_value(value_dict)
            self.ovalues = [ovalue]

    def append_value(self, value):
        self.ovalues.append(value)

    def get_value(self, raw=False):
        return [val.get_value(raw) for val in self.ovalues]

    def render(self, context):
        return "<br>".join([val.render(context) for val in self.ovalues])

    def delete(self):
        [val.delete() for val in self.ovalues]
        self.ovalues = []

class ObjectValueTaxo(object):
    """ This class generates fakes ObjectValue from taxo related object """

    @classmethod
    def taxo_values(cls, bo):
        """ Generates an ObjectValue for each rank of taxo property of bo and return the list of values """
        values = []
        if bo.taxon:
            for taxo in bo.taxon.get_parents():
                try:
                    values.append(ObjectValueTaxo(bo, taxo))
                except Attribute.DoesNotExist:
                    pass
        return values

    def __init__(self, bo, taxo):
        self.attr = Attribute.objects.get(name=taxo.get_attr_name())
        self.attr.is_editable = lambda s: False
        self.obj = bo
        self.value = taxo.name
        self.comments = None

    def __str__(self):
        return "%s: %s" % (self.attr.name, self.get_value())

    def get_value(self, raw=False):
        return self.value

    def render(self, context):
        return "%s" % self.get_value()


class ObjectValueStats(models.Model):
    """ This value is special enough to deserve its own structure (+attr_subj, -value_text, -value_voic) """
    obj = models.ForeignKey('BaseObject', on_delete=models.CASCADE)
    attr = models.ForeignKey('Attribute', related_name='stat_val', on_delete=models.CASCADE) # Attribute 'statistic'
    attr_subj = models.ForeignKey('Attribute', on_delete=models.CASCADE, related_name='target_val') # Attribute on which the statistic is targetting
    value_int   = models.IntegerField(blank=True, null=True, validators=[int_validator])
    value_float = models.FloatField(blank=True, null=True)
    comments    = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "object_value_stats"

    def __str__(self):
        return "%s: %s" % (self.attr.name, self.get_value())

    def _value_field(self):
        return self.attr.get_value_field()

    def set_value(self, value):
        setattr(self, self._value_field(), value)
        self.save()

    def get_value(self):
        return getattr(self, self._value_field())


class PhotoAttachment(models.Model):
    photo = ImageField(upload_to='photos')
    title = models.TextField(blank=True, verbose_name=_("Title"))
    order = models.IntegerField(default=0)
    # Generic relation fields
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()
    class Meta:
        db_table = "photoattachment"


class FileAttachment(models.Model):
    fil      = models.FileField(upload_to='attachments', verbose_name=_("File"))
    title    = models.TextField(blank=True, verbose_name=_("Title"))
    mimetype = models.CharField(max_length=100, blank=True, editable=False)
    # Generic relation fields
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()
    class Meta:
        db_table = "fileattachment"
    
    def get_icon(self):
        """ Return an icon from the file type, if available in STATIC_URL/img/mimetypes """
        img_abspath = finders.find(os.path.join('img','mimetypes', "%s.png" % self.mimetype.replace("/","-")))
        if img_abspath:
            return os.path.join(settings.STATIC_URL, 'img','mimetypes', "%s.png" % self.mimetype.replace("/","-"))
        return os.path.join(settings.STATIC_URL, 'img','mimetypes', 'unknown.png')
