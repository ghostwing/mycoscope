import io
import factory
from factory.django import DjangoModelFactory

from django.utils import timezone

from publications.models import Publication, PressCorner


class AbstractPublicationFactory(DjangoModelFactory):
    publishing_datetime = factory.LazyFunction(timezone.now)
    title = factory.Sequence(lambda n: "Title #%s" % n)
    summary = factory.Faker("sentence")
    link = factory.Faker("safe_domain_name")
    pdf_file = factory.django.FileField(
        from_func=lambda: io.BytesIO(b""), filename=factory.Faker("file_name")
    )


class PublicationFactory(AbstractPublicationFactory):
    class Meta:
        model = Publication

    author = factory.Faker("name")


class PressCornerFactory(AbstractPublicationFactory):
    class Meta:
        model = PressCorner
