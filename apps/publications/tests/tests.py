from freezegun import freeze_time

from django.conf import settings
from django.test import TestCase
from django.shortcuts import reverse

from publications.tests.factories import PublicationFactory, PressCornerFactory


class PublicationsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        if "policy.middleware.PolicyMiddleware" in settings.MIDDLEWARE:
            settings.MIDDLEWARE.remove("policy.middleware.PolicyMiddleware")

    def test_publihsed_publications_are_shown_in_time(self):
        with freeze_time("2024-01-01"):
            p1 = PublicationFactory()
        with freeze_time("2024-01-02"):
            p2 = PublicationFactory()

        with freeze_time("2023-12-31"):
            response = self.client.get(reverse("publications"))
            self.assertNotContains(response, p1.title)
            self.assertNotContains(response, p1.summary)
            self.assertNotContains(response, p2.title)
            self.assertNotContains(response, p2.summary)

        with freeze_time("2024-01-01"):
            response = self.client.get(reverse("publications"))
            self.assertContains(response, p1.title)
            self.assertContains(response, p1.summary)
            self.assertNotContains(response, p2.title)
            self.assertNotContains(response, p2.summary)

        with freeze_time("2024-01-02"):
            response = self.client.get(reverse("publications"))
            self.assertContains(response, p1.title)
            self.assertContains(response, p1.summary)
            self.assertContains(response, p2.title)
            self.assertContains(response, p2.summary)


class PressCornerTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        if "policy.middleware.PolicyMiddleware" in settings.MIDDLEWARE:
            settings.MIDDLEWARE.remove("policy.middleware.PolicyMiddleware")

    def test_published_press_corner_are_shown_in_time(self):
        with freeze_time("2024-01-01"):
            p1 = PressCornerFactory()
        with freeze_time("2024-01-02"):
            p2 = PressCornerFactory()

        with freeze_time("2023-12-31"):
            response = self.client.get(reverse("press_corner"))
            self.assertNotContains(response, p1.title)
            self.assertNotContains(response, p1.summary)
            self.assertNotContains(response, p2.title)
            self.assertNotContains(response, p2.summary)

        with freeze_time("2024-01-01"):
            response = self.client.get(reverse("press_corner"))
            self.assertContains(response, p1.title)
            self.assertContains(response, p1.summary)
            self.assertNotContains(response, p2.title)
            self.assertNotContains(response, p2.summary)

        with freeze_time("2024-01-02"):
            response = self.client.get(reverse("press_corner"))
            self.assertContains(response, p1.title)
            self.assertContains(response, p1.summary)
            self.assertContains(response, p2.title)
            self.assertContains(response, p2.summary)
