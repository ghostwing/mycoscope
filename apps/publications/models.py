from django.db import models
from django.db.models import F
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class PublicationManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .all()
            .order_by(F("publishing_datetime").desc(nulls_last=True))
        )


class PublishedManager(PublicationManager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(
                publishing_datetime__isnull=False,
                publishing_datetime__lte=timezone.now(),
            )
        )


class AbstractPublication(models.Model):
    class Meta:
        abstract = True

    objects = PublicationManager()
    objects_published = PublishedManager()

    publishing_datetime = models.DateTimeField(
        verbose_name=_("publishing datetime"), blank=True, null=True
    )
    title = models.CharField(verbose_name=_("title"))
    summary = models.TextField(verbose_name=_("summary"), blank=True)
    link = models.URLField(verbose_name=_("link"), blank=True)

    def __str__(self):
        return self.title


class Publication(AbstractPublication):
    pdf_file = models.FileField(
        verbose_name=_("pdf file"), blank=True, upload_to="publications/publications/"
    )
    author = models.CharField(verbose_name=_("author"), blank=True)


class PressCorner(AbstractPublication):
    pdf_file = models.FileField(
        verbose_name=_("pdf file"), blank=True, upload_to="publications/press_corner/"
    )
