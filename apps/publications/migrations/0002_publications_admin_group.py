# inspiration: https://gist.github.com/solace/6a8ac71539220b1f13a95bd559f2c4bd

from django.contrib.auth.models import Group, Permission
from django.core.management.sql import emit_post_migrate_signal
from django.db import migrations
import logging


logger = logging.getLogger(__name__)

role_permissions = {
    "myco_publications_admin": [
        "add_presscorner",
        "change_presscorner",
        "delete_presscorner",
        "view_presscorner",
        "add_publication",
        "change_publication",
        "delete_publication",
        "view_publication",
    ],
}


def add_role_permissions(apps, schema_editor):
    emit_post_migrate_signal(2, False, "default")
    for r in role_permissions:
        role, created = Group.objects.get_or_create(name=r)
        logger.info(f"{r} Role retrieved")
        for p in role_permissions[r]:
            perm, created2 = Permission.objects.get_or_create(codename=p)
            role.permissions.add(perm)
            logger.info(f"Permitting {r} to {p}")
        role.save()


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("contenttypes", "__latest__"),
        ("auth", "__latest__"),
        ("publications", "0001_initial"),
    ]

    operations = [
        migrations.RunPython(
            add_role_permissions, reverse_code=migrations.RunPython.noop
        ),
    ]
