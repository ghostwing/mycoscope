from django.views.generic import TemplateView
from django.shortcuts import reverse
from django.utils.translation import gettext as _

from .models import PressCorner, Publication

HOME_TUPLE = (_("Home"), "/")


class PublicationsView(TemplateView):
    template_name = "publications/publications.html"

    def get_context_data(self, **kwargs):
        title = _("Publications")
        context = {
            "breadcrumb": [HOME_TUPLE, (title, "")],
            "title": title,
            "publications": Publication.objects_published.all(),
        }
        if self.request.user.has_perm("publications.view_publication"):
            context["admin_link"] = reverse("admin:publications_publication_changelist")
        return context


class PressCornerView(TemplateView):
    template_name = "publications/publications.html"

    def get_context_data(self, **kwargs):
        title = _("Press Corner")
        context = {
            "breadcrumb": [HOME_TUPLE, (title, "")],
            "title": title,
            "publications": PressCorner.objects_published.all(),
        }
        if self.request.user.has_perm("publications.view_publication"):
            context["admin_link"] = reverse("admin:publications_presscorner_changelist")
        return context
