from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='Permission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.PositiveIntegerField()),
                ('perm', models.CharField(max_length=10, choices=[(b'read', 'Read'), (b'r+', 'Read and View private fields'), (b'rw', 'Modify')])),
                ('content_type', models.ForeignKey(related_name='permissions', to='contenttypes.ContentType', on_delete=models.CASCADE)),
                ('group', models.ForeignKey(to='auth.Group', on_delete=models.CASCADE)),
            ],
        ),
    ]
