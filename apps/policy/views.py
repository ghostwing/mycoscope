from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from django import forms


class PolicyForm(forms.Form):
    accept = forms.BooleanField(required=False, label=_("I accept these policies"))


def accept_policy(request):
    form = PolicyForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            if 'accept' in form.cleaned_data:
                response = HttpResponseRedirect(reverse('home'))
                response.set_cookie('policy_accepted', '1', max_age=5*365*24*3600)
                return response
    context = {
        'form': form,
    }
    return render(request, 'policy/policy.html', context)
