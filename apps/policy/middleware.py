from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import reverse


class PolicyMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        path = request.META['PATH_INFO']
        if path.startswith(settings.STATIC_URL) or "/login/" in path or "/by_uuid/" in path \
                or request.user.is_authenticated or 'policy_accepted' in request.COOKIES:
            return self.get_response(request)
        policy_url = reverse('accept_policy', args=[], current_app='policy')
        if not policy_url == path:
            return HttpResponseRedirect(policy_url)

        return self.get_response(request)
