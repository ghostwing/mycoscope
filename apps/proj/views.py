from django.conf import settings
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponseRedirect, HttpResponseServerError, Http404
from django.shortcuts import render
from django.template import loader
from django.urls import reverse
from django.utils.translation import gettext as _, check_for_language

HOME_TUPLE = (_("Home"), "/")
HELP_PAGES = {
    'external_data': _("How to import data in an external program"),
    'external_data_2007': _("How to import data in MS Excel 2007"),
}


def profile(request):
    disp_user = request.user
    context = {
        'disp_user': disp_user,
        'invents': disp_user.get_inventories().all(),
    }
    return render(request, ['proj/custom_profile.html', 'proj/profile.html'], context)


def site_register(request):
    raise Http404("Registration is currently disabled on this site. Write to the site administrators to get an account")
    if request.method == 'POST':
        form = RegistrationForm(data = request.POST)
        if form.is_valid():
            new_user = form.save()
            return HttpResponseRedirect(reverse('register_success'))
    else:
        form = RegistrationForm()
    context = {
        'form': form,
    }
    return render(request, 'proj/register.html', context)

def activate_account(request, key):
    """ Activate an account through the link a requestor has received by email """
    try:
        person = Person.objects.get(activation_key=key)
    except Person.DoesNotExist:
        return render(request, 'error.html', {'error':"Sorry, the key you provided is not valid."})
    person.activate()
    Person.clean_unactivated_accounts()
    return site_login(request, msgs=[_("Your account has been activated.")])

def set_language(request, lang):
    """ Copied from django/views/i18n.py, to be able to accept also GET requests """
    next = request.REQUEST.get('next', None)
    if not next:
        next = request.META.get('HTTP_REFERER', None)
    if not next:
        next = '/'
    response = HttpResponseRedirect(next)
    if request.method == 'GET':
        lang_code = lang
        if lang_code and check_for_language(lang_code):
            if hasattr(request, 'session'):
                request.session['django_language'] = lang_code
            else:
                response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang_code)
    return response


@permission_required('invent.add_baseobject')
def site_admin(request):
    context = {
        'breadcrumb' : [HOME_TUPLE, (_("Site Administration"), "")],
        'web_stats'  : settings.WEB_STATS_URL,
    }
    return render(request, 'proj/site_admin.html', context)


def help_index(request):
    """ Home page of help """
    context = {
        'breadcrumb' : [HOME_TUPLE, (_("Help"), "")],
        'pages'      : HELP_PAGES,
    }
    return render(request, 'help/index.html', context)

def help(request, subject):
    """ Display a help page on 'subject' """
    if not subject in HELP_PAGES:
        raise Http404
    context = {
        'breadcrumb' : [HOME_TUPLE, (_("Help"), reverse('help_index')), (HELP_PAGES[subject], "")],
    }
    return render(request, 'help/%s.html' % subject, context)

def server_error(request, template_name='500.html'):
    # http://www.djangosnippets.org/snippets/1199/
    t = loader.get_template(template_name)
    return HttpResponseServerError(t.render({'STATIC_URL': settings.STATIC_URL}))
