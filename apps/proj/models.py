from django.db import models
from django.contrib.auth.models import User


class BCISUser(User):
    # firstname, lastname and email are defined in the User model 
    address  = models.CharField(max_length=255, blank=True)
    postcode = models.CharField(max_length=12, default='')
    city     = models.CharField(max_length=50, default='')
    tel      = models.CharField(max_length=50, blank=True)
    fax      = models.CharField(max_length=50, blank=True)

    class Meta:
        ordering = ('last_name', 'first_name')

    def __str__(self):
        if self.first_name or self.last_name:
            return " ".join([self.first_name, self.last_name])
        else:
            return self.username
