from functools import total_ordering

from django.db import models
from django.conf import settings
from django.urls import reverse
from django.utils.translation import gettext, gettext_lazy as _
from django.utils.safestring import mark_safe
from django.utils.html import escape

# cf http://fr.wikipedia.org/wiki/Taxon
@total_ordering
class Rank:
    all_ranks = []
    
    def __init__(self, name, trans, letter):
        self.name = name
        self.trans = trans
        self.letter = letter
        self.order = len(self.all_ranks)
        self.all_ranks.append(self)

    def __repr__(self):
        return "Rank(%s, %s, %s)" % (self.name, self.trans, self.letter)
    
    def __eq__(self, other):
        return self.order == other.order

    def __lt__(self, other):
        return self.order < other.order

    def get_author_attribute_name(self):
        """ Return the attribute name corresponding to this rank, or the empty string """
        return {
            'genus'     : "GENUS_AUTHOR",
            'species'   : "SPAUTHOR", # EURISCO
            'subspecies': "SSPAUTHOR",
            'subtaxa'   : "SUBTAUTHOR", # EURISCO
            }.get(self.name, "")

    @classmethod
    def get(cls, name):
        for r in cls.all_ranks:
            if r.name == name:
                return r
        return None
    
    @classmethod
    def get_ranks_from_list(cls, rank_names):
        """ Given a list of rank names, returns a list of objects """
        rank_dict = dict([(r.name, r) for r in cls.all_ranks])
        return [rank_dict[name] for name in rank_names]

class RankList(object):
    """ Singleton class containing the rank hierarchy of BCIS """
    ranks = (
        Rank('regnum', _("Regnum"), 'R'),
        Rank('divisio', _("Divisio"), 'D'),
        Rank('classis', _("Classis"), 'C'),
        Rank('ordo', _("Ordo"), 'O'),
        Rank('familia', _("Familia"), 'F'),
        Rank('genus', _("Genus"), 'G'),
        Rank('species', _("species"), 'S'),
        Rank('subspecies', _("subspecies"), 'SS'),
        Rank('varietas', _("varietas"), 'V'),
        # EURISCO: Subtaxa can be used to store any additional taxonomic identifier, in latin.
        # Following abbreviations are allowed: ‘subsp.’ (for subspecies); ‘convar.’ (for convariety); ‘var.’ (for variety); ‘f.’ (for form).
        Rank('subtaxa', _("subtaxa"), 'ST'),
    )
    instance = None
    
    def __new__(cls, *args, **kargs): 
        if cls.instance is None:
            cls.instance = object.__new__(cls, *args, **kargs)
        return cls.instance

    @classmethod
    def get_rank_choices(cls):
        return [(r.name, r.trans) for r in cls.ranks]


STATUS_CHOICES = (
    ('A', _("Accepted")),
    ('S', _("Synonym")),
    ('C', _("Container")),
    ('I', _("Included")),
)

class TaxonManager(models.Manager):
    def with_hierarchy(self, count):
        """ Returns a query with 'count' levels of join to retrieve parents of taxons"""
        param = 'parent'
        args = [param]
        for i in range(1, count):
            param += '__parent'
            args.append(param)
        return self.select_related(*args)
    def get_by_sourceid(self, src_name, src_id):
        if isinstance(src_name, str):
            return self.get(sources__name=src_name, taxonsourcetaxon__id_in_source=src_id)
        else:
            return self.get(sources__name=src_name.name, taxonsourcetaxon__id_in_source=src_id)

class Taxon(models.Model):
    """ Generic class representing a taxon unit from a taxonomy (Familia, Genus, ...) """
    name    = models.CharField(max_length=60, db_index=True)
    rank    = models.CharField(max_length=15, db_index=True, choices=RankList.get_rank_choices())
    parent = models.ForeignKey('self', blank=True, null=True, on_delete=models.CASCADE, related_name='children')
    # This is a chain of rank uids from top to bottom : "F:A0|G:FF"
    hierarchy = models.CharField(max_length=100, default="", editable=False)
    author  = models.CharField(max_length=50, blank=True, null=True)
    commonname = models.CharField(max_length=100, blank=True, null=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default="A")
    syn_of = models.ForeignKey('self', blank=True, null=True, on_delete=models.CASCADE, related_name='synonyms')
    comment = models.CharField(max_length=100, blank=True, null=True)
    # Source(s) of data (ISFS, Mansfield, etc.)
    sources = models.ManyToManyField('TaxonSource', through='TaxonSourceTaxon')

    objects = TaxonManager()
    attr_to_rank_map = {'REGNUM':'regnum', 'PHYLUM':'divisio', 'CLASSIS':'classis', 'ORDO':'ordo',
                        'FAMILY' :'familia', 'GENUS':'genus', 'SPECIES': 'species', 'SUBSPECIES':'subspecies', 'VARNAME':'varietas'}
    rank_to_attr_map = {'regnum':'REGNUM', 'divisio':'PHYLUM', 'classis':'CLASSIS', 'ordo':'ORDO',
                        'familia':'FAMILY', 'genus':'GENUS', 'species': 'SPECIES', 'subspecies':'SUBSPECIES', 'varietas':'VARNAME'}
    
    class Meta:
        db_table = "taxon"
        ordering = ['name']
    
    def __str__(self):
        return "%s: %s" % (self.get_rank_display(), self.name)
    
    def get_absolute_url(self):
        return reverse('taxo_browse', args=[str(self.id)])

    @classmethod
    def get_from_data(cls, data, create_if_absent=False):
        """ data is a dict like {'FAMILY':?, 'GENUS':? ...}
            if create_if_absent is True, the taxon is created if not found in current taxonomy """
        data_by_rank = [(Rank.get(cls.attr_to_rank_map.get(k)), v) for k,v in data.items() if k in cls.attr_to_rank_map.keys()]
        data_by_rank.sort()
        tx = None
        for d in data_by_rank:
            tx_list = Taxon.objects.filter(rank=d[0].name, name=d[1])
            if tx:
                tx_list = tx_list.filter(hierarchy__contains=tx.get_hid())
            if tx_list.count() == 0:
                if create_if_absent:
                    new_tx = cls.create_taxon_from_data(data, tx, d)
                    tx_list = [new_tx]
                else:
                    raise ValueError(gettext("Unable to find %(rank)s '%(name)s' in %(taxon)s") % {
                        'name':d [1], 'rank': d[0].name, 'taxon': tx and tx.full_name or "taxonomy"})
            elif tx_list.count() > 1:
                # FIXME: Try to distinct by author
                raise ValueError(gettext("Multiple occurrences found for '%(name)s' in %(rank)s taxonomy") % {'name':d [1], 'rank': d[0].name})
            tx = tx_list[0]
        return tx

    @classmethod
    def create_taxon_from_data(cls, all_data, parent, rank_data):
        """ rank_data is a tuple in the form: (<Rank object>, 'Taxon name') """
        tx = Taxon(name=rank_data[1], rank=rank_data[0].name, parent=parent)
        tx.author = all_data.get(rank_data[0].get_author_attribute_name(), None)
        # FIXME: add supplementary data if last rank ? (CROPNAME VARCOMMONNAMEF...)
        tx.save()
        tx_source, created = TaxonSource.objects.get_or_create(name="custom")
        txt = TaxonSourceTaxon(taxon=tx, source=tx_source)
        txt.save()
        return tx

    def get_parents(self, stop_rank_name=None):
        """ Returns a list of parents in hierarchical order (top to bottom), with self at bottom """
        stop_rank = stop_rank_name and Rank.get(stop_rank_name) or None
        par_list = [self]
        obj = self
        while obj.parent:
            if stop_rank and obj.parent.get_rank_object() < stop_rank:
                break
            par_list.insert(0, obj.parent)
            obj = obj.parent
        # TODO: use mptt to reduce queries...
        #parents = self.get_ancestors()
        #to_return = parents.filter(lambda x: x.get_rank_object() >= stop_rank, parents)
        return par_list

    @property
    def full_name(self):
        """ Compose full name with parents hierarchy concatenated """
        return " ".join([tx.name for tx in self.get_parents()])
    
    def full_name_with_url(self):
        """ Return full name with each taxon linked to its browse page """
        return " ".join(["<a href='%s' class='taxo_link'>%s</a>" % (tx.get_absolute_url(), tx.name)for tx in self.get_parents()])

    def get_title(self):
        return self.name

    def get_id_in_source(self, source):
        """ Return the id_in_source property for the specified source object """ 
        try:
            return self.taxonsourcetaxon_set.get(source=source).id_in_source
        except:
            return None

    def get_children(self):
        return self.children.all()

    def get_rank_object(self):
        """ Return the Rank object corresponding to self.rank """
        for r in RankList.ranks:
            if self.rank == r.name:
                return r
        return None

    def get_attr_name(self):
        """ Return the attribute name corresponding to self.rank (mapping)"""
        return self.rank_to_attr_map.get(self.rank)

    def get_hid(self):
        """ Return hierarchy id (e.g. 'F:4D')"""
        return "%s:%X" % (self.get_rank_object().letter, self.id)
    
    def get_hierarchy_string(self):
        """ Sort of hash string composed by hierarchy to ease search of all children """
        if self.parent:
            parent_hierarchy = "%s|" % self.parent.get_hierarchy_string()
        else:
            parent_hierarchy = ""
        return "%s%s" % (parent_hierarchy, self.get_hid())
    
    def get_rank_name(self, rank):
        """ Returns the name of the rank 'rank' of this taxon, if available """
        return dict([(tx.rank, tx.name) for tx in self.get_parents()]).get(rank, None)

    def get_taxon_by_rank(self, rank_name):
        """ Returns the taxon object corresponding to rank in the taxon hierarchy """
        if self.rank == rank_name:
            return self
        elif self.get_rank_object() < Rank.get(rank_name):
            return None
        else:
            # look for rank_name in parent hierarchy
            parent_ranks = dict([(p.rank, p) for p in self.get_parents()])
            return parent_ranks.get(rank_name, None)

    def get_species_name(self):
        """ Return the name of the taxon from Genus and down """
        specname = " ".join([tx.name for tx in self.get_parents(stop_rank_name='genus')])
        return "%s %s" % (specname, self.author)

    def get_links(self):
        """ Depending on taxon sources, return links to external sites """
        links = []
        for source in self.sources.all():
            id_in_source = self.get_id_in_source(source)
            if source.retr_url and id_in_source:
                links.append(u"<a href='%(sp_url)s'><img src='%(img_url)s' alt='%(src_name)s logo' title='%(title)s' width='32'/></a>" % {
                              'sp_url':   source.retr_url % id_in_source,
                              'img_url':  source.logo.url,
                              'title':    escape(gettext("See this specimen's taxon on %s site") % source.name),
                              'src_name': source.name})
        return mark_safe(u"&nbsp;".join(links))


class TaxonTranslation(models.Model):
    """ Common name translations for taxons """
    taxon = models.ForeignKey(Taxon, on_delete=models.CASCADE)
    language = models.CharField(max_length=5, choices=settings.LANGUAGES)
    cn_trans = models.CharField(max_length=100)
    class Meta:
        db_table = "taxon_translation"
        unique_together = ('taxon', 'language')

    def __str__(self):
        return "%s in %s: %s" % (self.taxon.commonname, self.language, self.cn_trans)


class TaxonSource(models.Model):
    name = models.SlugField(max_length=20)
    # ex: http://www.crsf.ch/index.php?page=espece_carteatlas&no_isfs=%s
    retr_url = models.URLField()
    logo     = models.ImageField(upload_to="logos", null=True, blank=True)
    class Meta:
        db_table = "taxon_source"
    
    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,)

class TaxonSourceTaxon(models.Model):
    taxon = models.ForeignKey(Taxon, on_delete=models.CASCADE)
    source = models.ForeignKey(TaxonSource, on_delete=models.CASCADE)
    id_in_source = models.IntegerField(blank=True, null=True)
    class Meta:
        db_table = "taxon_source_taxon"
        unique_together = (("source", "id_in_source"),)

#def get_rank_content(rank, cur_taxon):
#    """ Return a list of all taxons from the 'rank'
#        If cur_taxon is defined, returns only child of cur_taxon """
#    selection = Taxon.objects.filter(rank=rank)
#    if cur_taxon:
#        # Limit query by cur_taxon
#        selection = selection.filter(hierarchy__startswith=cur_taxon.get_hierarchy_string())
#    return selection.all()

#def get_children(cur_taxon):
#    return

#def get_next_rank(rank):
#    next_is_good = False
#    for r in RANK_CHOICES:
#        if next_is_good:
#            return r[0]
#        if rank == r[0]:
#            next_is_good = True
#    return None

from django.db.models.signals import post_save
def populate_hierarchy(sender, instance, **kwargs):
    if instance.parent:
        hstring = instance.get_hierarchy_string()
        if instance.hierarchy != hstring:
            instance.hierarchy = hstring
            instance.save()

post_save.connect(populate_hierarchy, sender=Taxon)

