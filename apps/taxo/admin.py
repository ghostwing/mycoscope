from django.contrib import admin
from taxo.models import Taxon, TaxonSource, TaxonSourceTaxon, TaxonTranslation

class TaxonTranslationInline(admin.TabularInline):
    model = TaxonTranslation
    extra = 3

class TaxonSourceTaxonInline(admin.TabularInline):
    model = TaxonSourceTaxon
    extra = 1

class TaxonAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_filter = ('rank',)
    raw_id_fields = ('parent', 'syn_of')
    readonly_fields = ('hierarchy',)
    inlines = (TaxonTranslationInline, TaxonSourceTaxonInline)

admin.site.register(Taxon, TaxonAdmin)
admin.site.register(TaxonSource)

