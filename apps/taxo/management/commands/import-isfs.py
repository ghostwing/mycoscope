import os
import xlrd
# http://www.lexicon.net/sjmachin/xlrd.html
import ooolib
# http://ooolib.sourceforge.net/ / package python-ooolib

from django.core.management.base import BaseCommand
from django.conf import settings
from django.db import transaction

from taxo.models import Taxon, TaxonSource, TaxonSourceTaxon

COL_ISFS = 1
COL_STATUS = 2
COL_SYN_ID = 7
COL_NAME = 3
COL_FAMILY = 4
COL_GENUS = 14
COL_SPECIES = 15
COL_SUBSPECIES = 16
COL_VARIETY = 18
COL_AUTHOR = 17

src_name = 'SISF'

class Command(BaseCommand):
    """ Import the ISFS data from an Excel Worksheet """
    @transaction.commit_on_success
    def handle(self, *args, **options):
        #source_path = os.path.join(settings.MEDIA_ROOT, "taxo_sources", "ISFS2_20070313.xls")
        source_path = os.path.join(settings.MEDIA_ROOT, "taxo_sources", "ISFS2_20070313_RH_V1.ods")
        source_type = source_path[-3:]
        if source_type == "xls":
            book = xlrd.open_workbook(source_path)
            data_sheet = book.sheet_by_index(1)
            nrows = data_sheet.nrows
        elif source_type == "ods":
            book = ooolib.Calc(opendoc=source_path)
            book.set_sheet_index(0)
            data_sheet = book
            (ncols, nrows) = data_sheet.get_sheet_dimensions()
        
        kingdom, cr = Taxon.objects.get_or_create(name='Plantae', rank='regnum')
        taxon_source, cr = TaxonSource.objects.get_or_create(name=src_name)
        imported = 0
        syn_list = []
        for rx in range(nrows):
            if rx == 0:
                continue # Header row
            
            if source_type == "xls":
                row = data_sheet.row(rx)
                isfs_id, status, name, sp_author, family_name = int(row[COL_ISFS-1].value), row[COL_STATUS-1].value, row[COL_NAME-1].value, row[COL_AUTHOR-1].value, row[COL_FAMILY-1].value
                name_chunks = name.split()
                genus_name = name_chunks[0]
                sp_name = " ".join(name_chunks[1:])
            elif source_type == "ods":
                isfs_id = int(data_sheet.get_cell_value(COL_ISFS, rx+1)[1])
                status = data_sheet.get_cell_value(COL_STATUS, rx+1)[1]
                syn_of = data_sheet.get_cell_value(COL_SYN_ID, rx+1)
                syn_of = syn_of and int(syn_of[1])
                sp_author = data_sheet.get_cell_value(COL_AUTHOR, rx+1)
                sp_author = sp_author and sp_author[1].replace("&amp;", "&")
                family_name = data_sheet.get_cell_value(COL_FAMILY, rx+1)[1]
                genus_name = data_sheet.get_cell_value(COL_GENUS, rx+1)[1]
                sp_name = data_sheet.get_cell_value(COL_SPECIES, rx+1)[1]
                subsp_name = data_sheet.get_cell_value(COL_SUBSPECIES, rx+1)
                subsp_name = subsp_name and subsp_name[1]
                var_name = data_sheet.get_cell_value(COL_VARIETY, rx+1)
                var_name = var_name and var_name[1]
                
            family, cr = Taxon.objects.get_or_create(name=family_name, rank='familia', defaults={'parent': kingdom})
            if cr:
                src = TaxonSourceTaxon(source=taxon_source, taxon=family)
                src.save()
            genus, cr  = Taxon.objects.get_or_create(name=genus_name, rank='genus', parent=family)
            if cr:
                src = TaxonSourceTaxon(source=taxon_source, taxon=genus)
                src.save()
            
            if subsp_name or var_name:
                # get parent species
                sps = Taxon.objects.filter(name=sp_name, rank='species', parent=genus, sources__taxonsourcetaxon=taxon_source)
                if len(sps) == 0:
                    # Create a 'fake' species to be able to link the subspecies to it
                    species = Taxon(name=sp_name, rank='species', parent=genus, status='Z')
                    species.save()
                    src = TaxonSourceTaxon(source=taxon_source, taxon=species)
                    src.save()
                elif len(sps) == 1:
                    species = sps[0]
                else:
                    # Multiple parent target species (choose Accepted one, or first) 
                    species = None
                    for sp in sps:
                        if sp.status == 'A':
                            species = sp
                            break
                    if not species:
                        species = sps[0]
                if subsp_name:
                    rank = 'subspecies'
                    name = subsp_name
                else:
                    rank = 'varietas'
                    name = var_name
                tx = Taxon(name=name, rank=rank, parent=species, status=status, author=sp_author)
                tx.save()
            else:
                tx = Taxon(name=sp_name, rank='species', parent=genus, status=status, author=sp_author)
                tx.save()

            src = TaxonSourceTaxon(source=taxon_source, taxon=tx, id_in_source=isfs_id)
            src.save()
                
            if status == 'S':
                syn_list.append((tx.id, int(syn_of)))
            imported += 1
        
        # set synonyms id
        for tp in syn_list:
            tx = Taxon.objects.get(pk=tp[0])
            tx2 = Taxon.objects.get_by_sourceid(taxon_source.name, tp[1])
            tx.syn_of = tx2
            tx.save()
        
        print "Import of SISF Data completed, %d taxons imported." % imported

