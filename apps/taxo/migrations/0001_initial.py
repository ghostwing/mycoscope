from django.conf import settings
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Taxon',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=60, db_index=True)),
                ('rank', models.CharField(db_index=True, max_length=15, choices=[('regnum', 'Regnum'), ('divisio', 'Divisio'), ('classis', 'Classis'), ('ordo', 'Ordo'), ('familia', 'Familia'), ('genus', 'Genus'), ('species', 'species'), ('subspecies', 'subspecies'), ('varietas', 'varietas'), ('subtaxa', 'subtaxa')])),
                ('hierarchy', models.CharField(default='', max_length=100, editable=False)),
                ('author', models.CharField(max_length=50, null=True, blank=True)),
                ('commonname', models.CharField(max_length=100, null=True, blank=True)),
                ('status', models.CharField(default='A', max_length=1, choices=[('A', 'Accepted'), ('S', 'Synonym'), ('C', 'Container'), ('I', 'Included')])),
                ('comment', models.CharField(max_length=100, null=True, blank=True)),
                ('parent', models.ForeignKey(related_name='children', blank=True, to='taxo.Taxon', null=True, on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['name'],
                'db_table': 'taxon',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TaxonSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.SlugField(max_length=20)),
                ('retr_url', models.URLField()),
                ('logo', models.ImageField(null=True, upload_to='logos', blank=True)),
            ],
            options={
                'db_table': 'taxon_source',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TaxonSourceTaxon',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('id_in_source', models.IntegerField(null=True, blank=True)),
                ('source', models.ForeignKey(to='taxo.TaxonSource', on_delete=models.CASCADE)),
                ('taxon', models.ForeignKey(to='taxo.Taxon', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'taxon_source_taxon',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TaxonTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language', models.CharField(max_length=5, choices=settings.LANGUAGES)),
                ('cn_trans', models.CharField(max_length=100)),
                ('taxon', models.ForeignKey(to='taxo.Taxon', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'taxon_translation',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='taxontranslation',
            unique_together=set([('taxon', 'language')]),
        ),
        migrations.AlterUniqueTogether(
            name='taxonsourcetaxon',
            unique_together=set([('source', 'id_in_source')]),
        ),
        migrations.AddField(
            model_name='taxon',
            name='sources',
            field=models.ManyToManyField(to='taxo.TaxonSource', through='taxo.TaxonSourceTaxon'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taxon',
            name='syn_of',
            field=models.ForeignKey(related_name='synonyms', blank=True, to='taxo.Taxon', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
